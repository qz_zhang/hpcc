#ifndef _CC_EDGE_H
#define _CC_EDGE_H
#include <memory>
#include <vector>
#include "gvar.h"

class Edge;
typedef std::shared_ptr<Edge> EdgeSharedPtr;

class Edge {
public:
  Edge();

  Edge(int _from, int _to, int _weight=1);

  void print() const;

  std::string toString() const;

  std::vector<int> getEndPoints();

  bool isLabeled();

  void markLabel(EdgeSharedPtr label=nullptr);

  EdgeSharedPtr getLabel();

  void setForestIndex(int index = -1);

  int getForestIndex();

public:
  /** data member **/
  int from_;
  int to_;
  int weight_;
  int forest_index_;
  EdgeSharedPtr label_;
};
class Edge;
typedef std::shared_ptr<Edge> EdgeSharedPtr;

struct EdgePtrCompare {
  bool operator()(EdgeSharedPtr left, EdgeSharedPtr right) const {
    return left->weight_ < right->weight_ || 
            left->weight_ == right->weight_ && left->from_ < right->from_ || 
            left->weight_ == right->weight_ && left->from_ == right->from_ && left->to_ < right->to_;
  }
};


#endif