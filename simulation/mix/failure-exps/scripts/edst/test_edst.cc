#include <stdio.h>
#include <set>
#include "union_find.h"
#include "edge.h"
#include "edst_solver.h"

void test_union_find_set() {
    //
    UnionFindSet ufs;
    ufs.init(10);
    int a = 1, b = 2;
    if (ufs.isInSameSet(a, b)) {
        printf("%d %d are in the same set!\n", a, b);
    }
    ufs.merge(a, b);
    if (ufs.isInSameSet(a, b)) {
        printf("%d %d are in the same set!\n", a, b);
    }
}

void test_edge() {
    std::set<EdgeSharedPtr, EdgePtrCompare> edges;
    edges.insert(std::shared_ptr<Edge>(new Edge(1, 2)));
    edges.insert(std::shared_ptr<Edge>(new Edge(2, 1, 2)));
    edges.insert(std::shared_ptr<Edge>(new Edge(2, 1, 1)));
    for(auto e : edges) {
        e->print();
    }
}

void test_edst_solver() {
    //
    clock_t begin = clock(), end;
    std::string topo_file = "../disjoint-path/topo/fc_100_3";
    EdstSolver sovler(topo_file, 20);
    sovler.solve();
    end = clock();
    printf("Edst run: %ds\n", (end - begin) / CLOCKS_PER_SEC);
}

int main(int argc, char **argv) {
    // test_union_find_set();
    // test_edge();
    test_edst_solver();
    return 0;
}