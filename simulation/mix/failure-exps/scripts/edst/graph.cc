
#include <fstream>
#include "graph.h"
#include "gvar.h"

Graph::Graph() { adjacent_matrix_.clear(); }

void Graph::constructGraph(const std::string &mat_file) {
    std::ifstream ifs;
    ifs.open(mat_file.c_str());
    int host_per_switch, switch_num, sw2sw_link_num, total_hosts;

 
    ifs >> host_per_switch >> switch_num >> sw2sw_link_num >> total_hosts;

    vertex_num_ = switch_num;

    int num_virtual_layers;
    ifs >> num_virtual_layers;
    std::vector<int> ports_of_virtual_switch;
    ports_of_virtual_switch.resize(num_virtual_layers, -1);

    for (int i = 0; i < num_virtual_layers; i++) {
        ifs >> ports_of_virtual_switch[i];
    }

    /*** A demo graph ***/
#ifdef EDST_DEBUG
    std::vector<std::vector<int> > demo = {
      {0, 1, 1, 1, 1},
      {1, 0, 1, INF, 1},
      {1, 1, 0, 1, INF},
      {1, INF, 1, 0, 1},
      {1, 1, INF, 1, 0}
    };

    for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 5; j++) {
        vertex_mapped_edges[i][j] = demo[i][j];
      }
    }
#endif
    adjacent_matrix_.resize(switch_num, std::vector<int >(switch_num, INF));
    for (int i = 0; i < sw2sw_link_num; i++) {
      int n1, n2, p1, p2;
      ifs >> n1 >> n2 >> p1 >> p2;
      adjacent_matrix_[n1][n2] = 1;
      adjacent_matrix_[n2][n1] = 1;
    }
    ifs.close();
    printf("read graph over...\n");
}

int Graph::getVertexNum() const {
    return vertex_num_;
}

void Graph::insertNonDecreasingEdges(std::set<EdgeSharedPtr, EdgePtrCompare > & edges) {
    for (int i = 0; i < vertex_num_; i++)
        for (int j = 0; j < vertex_num_; j++)
        if (j > i && adjacent_matrix_[i][j] == 1) {
            edges.insert(std::shared_ptr<Edge>(new Edge(i, j, 1) ) );
    }
}
