#include <vector>
#include <iostream>
#include <set>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <memory>
#include <fstream>
#include <queue>
#include <unordered_map>
#include <algorithm>
#include <stack>
#include <fstream>

#include "edst_solver.h"
#include "gvar.h"
// #define EDST_DEBUG


class RouteManager {
public:
  RouteManager(const std::string &mat_file, int k) 
    : mat_file_(mat_file), edst_solver_(new EdstSolver(mat_file, k)) {}

  void calPath(std::unordered_map<uint32_t, 
	                std::unordered_map<uint32_t, 
		                std::vector<std::vector<int> > > >& sw_paths) {
    edst_solver_->solve();
    vertex_num_ = edst_solver_->getVertexNum();
    std::cout << "shortest-path on edst\n";
    
    // auto matrix = edst_solver_->forestMatrix(0);
    // for (auto& vec : matrix) {
    //   FOR_EACH(vec);
    // }
    for (int f = 0; f < K; f++) {
      for (int i = 0; i < vertex_num_; i++) {
        dijkstraShortestPath(edst_solver_->forestMatrix(f), sw_paths, i);
      }
    }

// #ifdef EDST_DEBUG
    int pairs = 0;
    double minSumPathLen = 0;
    double sumAvgPathLen = 0;
    double sumPaths = 0;
    for (int i = 0; i < vertex_num_; i++) {
      for (int j = 0; j < vertex_num_; j++) {
        if (i == j) continue;
        ++pairs;
        sumPaths += sw_paths[i][j].size();
        
        int mini = 0xff;
        double tmpSum = 0;
        for (auto & path : sw_paths[i][j]) {
          // FOR_EACH(path);
          if (mini > path.size()) mini = path.size();
          tmpSum += path.size();
        }
        minSumPathLen += mini;
        sumAvgPathLen += (double)tmpSum / sw_paths[i][j].size();
        // printf("\n");
      }
    }
// #endif

    printf("Avg paths: %.2f, avg path lens: %.2f, avg minimum path lens: %.2f\n", sumPaths / pairs, sumAvgPathLen / pairs, minSumPathLen / pairs);
  };

private:
  void dijkstraShortestPath(const std::vector<std::vector<int> >& forest, 
                          std::unordered_map<uint32_t, 
	                          std::unordered_map<uint32_t, 
		                          std::vector<std::vector<int> > > >& sw_paths, 
                          int start) {
    std::vector<int> dis(vertex_num_, INF);
    std::vector<int> prev(vertex_num_, -1);
    std::vector<int> visited(vertex_num_, 0);
    dis[start] = 0;
    prev[start] = start;
    // visited[start] = 1;

    typedef std::pair<int, int> DisVexPair;
    std::priority_queue<DisVexPair, std::vector<DisVexPair>, std::greater<DisVexPair> > pq;
    pq.push({dis[start], start});
    while(!pq.empty()) {
      auto p = pq.top();
      pq.pop();
      int u = p.second;
      // std::cout << "u " << u << "dis[u]: " << dis[u] << "\n";
      if (visited[u])
        continue;
      visited[u] = 1;
      for (int v = 0; v < vertex_num_; v++) {
        if (!visited[v] && dis[v] >= dis[u] + forest[u][v]) {
          dis[v] = dis[u] + forest[u][v];
          prev[v] = u;
          pq.push({dis[v], v});
        }
      }
    }

    for (int end = 0; end < vertex_num_; end++) {
      if (end == start) continue;
      std::vector<int> path;
      int temp = end;
      while(prev[temp] != temp) {
        path.push_back(temp);
        temp = prev[temp];
      }
      path.push_back(start);
      std::reverse(path.begin(), path.end());
      sw_paths[start][end].push_back(path);
    }
  }
private:
  int vertex_num_;
  const std::string& mat_file_;
  std::shared_ptr<EdstSolver> edst_solver_;
};


std::string VecToString(const std::vector<int>& vec) {
  std::string res = "";
  for (int i = 0; i < vec.size(); i++) {
    res += std::to_string(vec[i]);
    if (i < vec.size() - 1) res += " ";
  }
  // if (res.length() > 256) {
  //   std::cout << "length > 256\n";
  // }
  return res;
}

std::unordered_map<uint32_t, 
  std::unordered_map<uint32_t, 
    std::vector<std::vector<int> > > > sw_paths; // sw id start from zero
void testRouteMgr(const std::string& mat_dir, const std::string& path_dir, int k) {
  clock_t bg, ed;

  RouteManager mgr(mat_dir, k);
  bg = clock();
  std::cout << "Calculating...\n";
  mgr.calPath(sw_paths);
  ed = clock();
  std::cout << "EDST Runinng: " << (double)(ed - bg) / CLOCKS_PER_SEC << "\n";

  std::ofstream ofs(path_dir);
  
  char strBuf[4096];
  std::ifstream ifs(mat_dir);
  int switches=64, to_host=4;
  ifs >> to_host >> switches;
  ifs.close();
  sprintf(strBuf, "%d %d %d\n", switches * (switches - 1), to_host, switches);
  ofs << strBuf;

  std::cout << "Write Path to File...\n";
  for (int i = 0; i < switches; i++) {
    for (int j = 0; j < switches; j++) {
      if (i == j) continue;
      sprintf(strBuf, "%d %d %d\n", i, j, sw_paths[i][j].size());
      ofs << strBuf;

      for (const auto &path : sw_paths[i][j]) {
        sprintf(strBuf, "%d %s\n", path.size(), VecToString(path).c_str());
        ofs << strBuf;
      }
    }
  }
  return;
}

int main(int argc, char **argv) {
  
  if (argc < 4) {
    std::cerr << "ERROR !!! USAGE: ./edst [MAT_DIR] [PATH_DIR] [K]\n";
    exit(-1);
  }
  std::string mat_dir(argv[1]);
  std::string path_dir(argv[2]);
  K = std::atoi(argv[3]);
  printf("TOPOLOGY_FILE: %s PATH_OUTPUT_FILE: %s K=%d\n", argv[1], argv[2], K);
  // // testParent();
  // solver.solve();
  // std::string mat_dir = "../disjoint-path/topo/fc_3000_4";
  // std::string path_dir = "../disjoint-path/path/edst_3000_4";
  // K = 20;
  testRouteMgr(mat_dir, path_dir, K);
  // testRouteMgr("../disjoint-path/topo/fc_2000_4", "../disjoint-path/path/edst_2000_4", K);
  // testRouteMgr("../disjoint-path/topo/fc_3000_4", "../disjoint-path/path/edst_3000_4", K);
  return 0;
}