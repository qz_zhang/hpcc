#include "forest.h"
#include <algorithm>
#include <iostream>

Forest::Forest(int vertexes, int id) 
    : partition_(new UnionFindSet() ), 
    vertex_num_(vertexes), forest_id_(id) 
{ 
    edges_.clear();
    
    partition_->init(vertex_num_); 
    edge_map_.clear();
    // forest_.resize(vertex_num_, std::vector<int>(vertex_num_, INF) );
    // for (int i = 0; i < vertex_num_; i++) {
    //   forest_[i][i] = 0;
    // }
}

void Forest::addEdge(EdgeSharedPtr e) { 
    edges_.push_back(e); 
    int a = e->getEndPoints()[0], b = e->getEndPoints()[1];
    e->setForestIndex(forest_id_);
    edge_map_[a][b] = e;
    edge_map_[b][a] = e;

    vertex_to_adjacent_edges_[a].push_back(b);
    vertex_to_adjacent_edges_[b].push_back(a);
    merge(a, b);
}

// void list_remove(std::list<int>& li, int removed_val) {
//     auto it = std::find(li.begin(), li.end(), removed_val);
//     if (it == li.end()) {
//         std::cerr << "err in list remove function\n";
//         exit(-1);
//     }
//     li.erase(it);
// }

void Forest::eraseEdge(EdgeSharedPtr e) {
    //
    e->setForestIndex(0);
    auto it = std::find(edges_.begin(), edges_.end(), e);
    if (it != edges_.end()) {
        edges_.erase(it);
    }
    int a = e->getEndPoints()[0], b = e->getEndPoints()[1];
    edge_map_[a][b] = nullptr;
    edge_map_[b][a] = nullptr;

    // remove
    vertex_to_adjacent_edges_[a].remove(b);
    vertex_to_adjacent_edges_[b].remove(a);
}

void Forest::passbyPath(
    std::vector<int>& passByNodes, 
    std::vector<EdgeSharedPtr> passByEdges, 
    int start, int end
) {
    std::vector<bool> visited(vertex_num_, false);
    std::vector<int> paths;
    visited[start] = true;
    // printf("MATRIX:\n");
    // for (auto row : forest_) {
    //   FOR_EACH(row);
    // }
    paths.push_back(start);
    uniquePath(passByNodes, passByEdges, paths, visited, start, end, start);
}

int Forest::find(int x) {
    return partition_->find(x);
}

void Forest::merge(int from, int to) {
    partition_->merge(from, to);
}

void Forest::markLabel(int x, int y, EdgeSharedPtr label) {
    edge_map_[x][y]->markLabel(label);
}

bool Forest::isLabeled(int x, int y) {
    return edge_map_[x][y]->isLabeled();
}

EdgeSharedPtr Forest::getMappedEdge(int x, int y) {
    return edge_map_[x][y];
}

void Forest::clearEdgeLabel() {
    #ifdef EDST_DEBUG
    std::cout << "clearing forest: " << forest_id_ << " edges num: " << edges_.size() << "\n";
    #endif
    for (auto edge : edges_) {
        edge->markLabel(nullptr);
    }
}

void Forest::debugForest() {
    for (auto e : edges_) {
        std::cout << "forest: " << forest_id_ << " edge: " << e->toString() << "\n";
    }
}

std::vector<std::vector<int> > Forest::getForestMatrix() {
    std::vector<std::vector<int> > forest_matrix(vertex_num_, std::vector<int>(vertex_num_, INF));
    for (auto e : edges_) {
        auto eps = e->getEndPoints();
        forest_matrix[eps[0]][eps[1]] = 1;
        forest_matrix[eps[1]][eps[0]] = 1;
    }
    return forest_matrix;
}


void Forest::uniquePath(
    std::vector<int>& passByNodes,
    std::vector<EdgeSharedPtr>& passByEdges,
    std::vector<int>& paths, 
    std::vector<bool>& visited,
    int src, int dst, int curNode
) {
    if (curNode == dst) {
      // std::cout << "find\n";
      // FOR_EACH(paths);
      passByNodes.assign(paths.begin(), paths.end());
      for (int i = 0; i < paths.size() - 1; i++) {
        passByEdges.push_back(edge_map_[paths[i]][paths[i + 1]]);
      }
      return;
    }
    for (auto node : vertex_to_adjacent_edges_[curNode]) {
        if (visited[node]) {
            continue;
        }
        visited[node] = true;
        paths.push_back(node);
        // FOR_EACH(paths)
        uniquePath(passByNodes, passByEdges, paths, visited, src, dst, node);
        visited[node] = false;
        paths.pop_back();
    }
}
