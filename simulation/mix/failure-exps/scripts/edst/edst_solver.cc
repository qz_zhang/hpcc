#include <vector>

#include "edst_solver.h"
#include "gvar.h"
#include <iostream>
#include <algorithm>


int parent(int x, std::vector<int>& nodes) {
  auto it = std::find(nodes.begin(), nodes.end(), x);
  if (it == nodes.begin()) {
    return *it;
  } else if (it == nodes.end()) {
    return -1;
  } else {
    return *(--it);
  }
}

EdstSolver::EdstSolver(const std::string& mat_file, int k)
  : g_(new Graph()),
  partition_zero_(std::unique_ptr<UnionFindSet>(new UnionFindSet() ) ) 
{
  g_->constructGraph(mat_file);
  partition_zero_->init(g_->getVertexNum());
  // initilization
  k_ = k;
  std::cout << "k " << k << "\n";
  for (int i = 0; i < k_; i++) {
    k_forests_.push_back( std::shared_ptr<Forest> (new Forest(g_->getVertexNum(), i + 1 ) ) );
  }
  g_->insertNonDecreasingEdges(increasing_edges_);

  #ifdef EDST_DEBUG
  std::cout << "forests num: " << k_forests_.size() << "\n";
  // initialize
  for (auto &inc : increasing_edges_) {
    std::cout << inc->toString() << "\n";
  }
  printf("construct graph over\n\n");
  #endif
}

void EdstSolver::solve() {
  // K = k_;
  printf("solve increasing_edges_ size=%d\n", increasing_edges_.size());
  while(!increasing_edges_.empty()) {
    EdgeSharedPtr e = *(increasing_edges_.begin());
    
    increasing_edges_.erase(increasing_edges_.begin());
    std::vector<int> endPoints = e->getEndPoints();
    if (increasing_edges_.size() % 10000 == 0) {
      printf("edges size: %d\n", increasing_edges_.size());
    }
    // if (0) { //  process next edge
    //   continue;
    // } else {
      labeling(e);
      if (!agumenting_sequence_.empty()) {
        agumenting();
      } else {
        partition_zero_->merge(endPoints[0], endPoints[1]);
      }
  //   }
  }
}


void EdstSolver::labeling(EdgeSharedPtr eRoot) {
  // clearing edges' label of forest
  clearLabel();
  // debug();
  #ifdef EDST_DEBUG
    labelingDebug();
  #endif

  while (!q_.empty())
    q_.pop();
  
  agumenting_sequence_.clear();

  // initialization
  q_.push(eRoot);
  // forest initialization
  while(1) {
    if (q_.empty()) {
      break;
    }
    EdgeSharedPtr e = q_.front();
    q_.pop();
    // std::cout << "label edge: " << e->toString() << "\n";
    const std::vector<int>& endPoints = e->getEndPoints();
    int x = endPoints[0];
    int y = endPoints[1];
    
    int i = e->getForestIndex();
    int iplus = I_PLUS(i) - 1;
    
    #ifdef EDST_DEBUG
    std::cout << "edge: " << e->toString() << " forestIdx= " << i << " iplus: " << iplus << "\n";
    #endif

    if  (k_forests_[iplus]->find(x) != k_forests_[iplus]->find(y)) {
      // agumenting sequence
      traceOutAgumentationSequence(eRoot, e);
      return;
    } else {
      std::vector<int > nodes;
      std::vector<EdgeSharedPtr > edges;  
      k_forests_[iplus]->passbyPath(nodes, edges, x, y);
      for (int i = 1; i < nodes.size(); i++) {
        int u = nodes[i];
        // int p = parent(u, nodes);
        int p = nodes[i - 1];
        if (!k_forests_[iplus]->isLabeled(u, p)) {
          k_forests_[iplus]->markLabel(u, p, e);
          q_.push(k_forests_[iplus]->getMappedEdge(u, p));
        }
      }

    }
  }
  return;
}


void EdstSolver::agumenting() {
  // add to forest
  if (agumenting_sequence_.size() == 1) {
    //
    k_forests_[0]->addEdge(agumenting_sequence_[0]);
  } else {
    //
    int l = agumenting_sequence_.size() - 1;
    for (int i = 0; i < l; i++) {
      int iplus = I_PLUS(i) - 1;
      k_forests_[iplus]->addEdge(agumenting_sequence_[i]);
      k_forests_[iplus]->eraseEdge(agumenting_sequence_[i + 1]);
    }
    k_forests_[I_PLUS(l) - 1]->addEdge(agumenting_sequence_[l]);
  }
  #ifdef EDST_DEBUG
  printf("after augmenting() forests\n");
  debug();
  printf("\n");
  #endif
}

void EdstSolver::clearLabel() {
  for (auto forest : k_forests_) {
    forest->clearEdgeLabel();
  }
}

void EdstSolver::traceOutAgumentationSequence(EdgeSharedPtr e, EdgeSharedPtr e_win) {
  int j = 0;
  agumenting_sequence_.push_back(e_win);
  while(agumenting_sequence_[j] != e) {
    agumenting_sequence_.push_back(agumenting_sequence_[j]->getLabel());
    j++;
  }
  std::reverse(agumenting_sequence_.begin(), agumenting_sequence_.end());
}

void EdstSolver::debug() {
  for (auto forest : k_forests_) {
    forest->debugForest();
  }
}

void EdstSolver::labelingDebug() {
  for (auto forest : k_forests_) {
    forest->debugForest();
  } 
}

