#ifndef _CC_FOREST_H
#define _CC_FOREST_H

#include "edge.h"
#include "union_find.h"
#include <list>
#include <unordered_map>
class Forest {
public:
    Forest(int vertexes, int id);

    void addEdge(EdgeSharedPtr e);

    void eraseEdge(EdgeSharedPtr e);

    // std::vector<EdgeSharedPtr>& getEdges();

    void passbyPath(std::vector<int>& passByNodes, 
                  std::vector<EdgeSharedPtr> passByEdges, 
                  int start, int end);

    int find(int x);

    void merge(int x, int y);

    void markLabel(int x, int y, EdgeSharedPtr label=nullptr);
    
    bool isLabeled(int x, int y);

    EdgeSharedPtr getMappedEdge(int x, int y);

    void clearEdgeLabel();
    
    void debugForest();

    std::vector<std::vector<int> > getForestMatrix();

private:
    void uniquePath(std::vector<int>& passByNodes,
                  std::vector<EdgeSharedPtr>& passByEdges,
                  std::vector<int>& paths, 
                  std::vector<bool>& visited,
                  int src, int dst, int curNode);
private:
    // test whether two points are in the same tree
    std::shared_ptr<UnionFindSet > partition_; 
    std::unordered_map<int, std::unordered_map<int, EdgeSharedPtr> > edge_map_;
    std::list<EdgeSharedPtr > edges_; 
    std::unordered_map<int, std::list<int> > vertex_to_adjacent_edges_; // may have many trees.
    int vertex_num_;
    int forest_id_;
};



#endif