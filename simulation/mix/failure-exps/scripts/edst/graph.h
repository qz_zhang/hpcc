#ifndef _CC_GRAPH_H
#define _CC_GRAPH_H
#include <unordered_map>
#include <vector>
#include <string>
#include <set>

#include "edge.h"
class Graph {
public:
    Graph();

    void constructGraph(const std::string &mat_file);
    
    int getVertexNum() const;

    void insertNonDecreasingEdges(std::set<EdgeSharedPtr, EdgePtrCompare > & edges);
private:
  int vertex_num_; // from zero to m_vertexNum
  std::vector <std::vector<int > > adjacent_matrix_;
};




#endif