#ifndef _CC_GVAR_H
#define _CC_GVAR_H

// #define EDST_DEBUG
#define INF 0xfffffff

#define FOR_EACH(vec) {\
      for (int l = 0; l < vec.size(); l++) { \
          if (l == vec.size() - 1) { if (vec[l] == INF) std::cout << "INF"; else std::cout << vec[l];} \
          else { if (vec[l] == INF) std::cout << "INF "; else std::cout << vec[l] << " "; } } \
          std::cout << "\n";\
      }

extern int K;

int I_PLUS(int i);
#endif