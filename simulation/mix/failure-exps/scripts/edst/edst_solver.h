#ifndef _CC_EDST_SOVLER_H
#define _CC_EDST_SOVLER_H

#include "edge.h"
#include "union_find.h"
#include "forest.h"
#include "graph.h"
#include <queue>

class EdstSolver {
  //
public:
  EdstSolver() { }
  EdstSolver(const std::string& mat_file, int k);
  void solve();
  void updateEdgeDisjointForests();
  void labeling(EdgeSharedPtr eRoot);
  void agumenting();

  int getVertexNum() {
    return g_->getVertexNum();
  }

  std::vector<std::vector<int > > forestMatrix(int forestIdx) {
    return k_forests_[forestIdx]->getForestMatrix();
  }

private:
  void clearLabel();
  void traceOutAgumentationSequence(EdgeSharedPtr e, EdgeSharedPtr e_win);
  void debug();
  void labelingDebug();
  


private:
  std::unique_ptr<UnionFindSet>  partition_zero_;

  std::vector<std::shared_ptr<Forest > > k_forests_;
  std::unique_ptr<Graph> g_;
  std::set<EdgeSharedPtr, EdgePtrCompare > increasing_edges_;
  std::queue<EdgeSharedPtr > q_;
  std::vector<EdgeSharedPtr > agumenting_sequence_; 
  int k_;
};

#endif