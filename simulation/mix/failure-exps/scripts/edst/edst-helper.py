import os
from multiprocessing import Process
def edst_path_calculation(mat_dir, path_dir, k):
    os.system("./edst %s %s %d" % (mat_dir, path_dir, k))
if __name__ == "__main__":
    print("CALCULATE EDST PATHS")
    os.system('make')
    path_processes = []
    path_processes.append(Process(target=edst_path_calculation, args=("../disjoint-path/topo/fc_500_4", "../disjoint-path/path/edst_500_4", 20 )))
    path_processes.append(Process(target=edst_path_calculation, args=("../disjoint-path/topo/fc_1000_4", "../disjoint-path/path/edst_1000_4",  20 )))
    path_processes.append(Process(target=edst_path_calculation, args=("../disjoint-path/topo/fc_5000_5", "../disjoint-path/path/edst_5000_5", 20 )))
    
    for p in path_processes:
        p.start()

    for p in path_processes:
        p.join()