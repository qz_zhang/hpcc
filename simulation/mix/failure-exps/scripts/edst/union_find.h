#ifndef _CC_UNION_FIND_H
#define _CC_UNION_FIND_H

#include <vector>

class UnionFindSet {
public:
  UnionFindSet();
  
  void init(int size);

  int find(int x);

  void merge(int from, int to);

  bool isInSameSet(int x, int y);

private:
  int set_size_;
  std::vector<int> parents_;
  std::vector<int> ranks_;
};

#endif