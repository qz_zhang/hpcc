#include "union_find.h"

UnionFindSet::UnionFindSet():set_size_(0) { 
    parents_.clear(); 
    ranks_.clear(); 
}

void UnionFindSet::init(int size) {
    set_size_ = size;
    parents_.resize(set_size_, -1);
    ranks_.resize(set_size_, 1);
    for(int i = 0; i < set_size_; i++) {
      parents_[i] = i;
    }
}

int UnionFindSet::find(int x) {
    return x == parents_[x] ? x : ( parents_[x] = find(parents_[x]) );
}

void UnionFindSet::merge(int from, int to) { // union (from set) to (to set)
    int x = find(from);
    int y = find(to);
    if (ranks_[x] <= ranks_[y]) {
        //
        parents_[x] = y;
    } else {
        parents_[y] = x;
    }
    if (ranks_[x] == ranks_[y] && x != y) {
        ranks_[y]++;
    }
}

bool UnionFindSet::isInSameSet(int x, int y) {
    return find(x) == find(y);
}
