#include "edge.h"
#include "gvar.h"
#include <string>

Edge::Edge(int _from, int _to, int _weight) 
    : from_(_from), 
    to_(_to), 
    weight_(_weight),
    label_(nullptr),
    forest_index_(0) {}

Edge::Edge()  
    : from_(-1), 
    to_(-1), 
    weight_(-1),
    label_(nullptr),
    forest_index_(0) {}

#ifdef EDST_DEBUG
Edge::~Edge() { 
    std::cout << " delete edge: " << toString() << "\n"; 
}
#endif

void Edge::print() const { 
    printf("%d->%d: %d\n", from_, to_, weight_); 
}

std::string Edge::toString() const { 
    char buf[20];
    snprintf(buf, 20, "(%d, %d)", from_, to_);
    return std::string(buf);
}

std::vector<int> Edge::getEndPoints() {
    return {from_, to_};
}

bool Edge::isLabeled() {
    return label_ != nullptr;
}

void Edge::markLabel(EdgeSharedPtr label) {
    label_ = label;
}

EdgeSharedPtr Edge::getLabel() {
    return label_;
}

void Edge::setForestIndex(int index) {
    if (index == -1) {
        return;
    }
    forest_index_ = index;
}

int Edge::getForestIndex() {
    return forest_index_;
}
