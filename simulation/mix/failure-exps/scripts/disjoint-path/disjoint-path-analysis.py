import time

def read_path(path_file='path'):
    path_f = open(path_file, mode='r')
    lines = path_f.read().splitlines()
    path_f.close()
            
    idx = 0
    l0 = [int(x) for x in lines[idx].split(' ')]
    pairs = l0[0]
    host_per_sw = l0[1]
    switches = l0[2]
    # print("sw", switches)

    path_pair = {}
    for i in range(switches):
        path_pair[i] = {}
        for j in range(switches):
            path_pair[i][j] = []
            

    idx += 1
    for _ in range(pairs):
        l = [int(x) for x in lines[idx].split(' ')]
        idx +=1
        src = l[0]
        dst = l[1]
        path_num = l[2]
        for _ in range(path_num):
            path_pair[ src ][ dst ].append([int(x) for x in lines[idx].split(' ')][ 1 : ])
            idx += 1
    return path_pair

def cmp(x, y):
    return len(x) > len(y)

def get_metric(switch, k):
    # path_f = path_fmt % (switch, k)
    path_fmt = "path/disjoint_%d_%d"
    path_f = path_fmt % (switch, k)
    path_pair = read_path(path_f)
    
    pairs = 0
    minimum_number_of_path = 0xffffff
    sum_lengths = 0
    num_paths = 0
    for src in range(switch):
        for dst in range(switch):
            if src == dst: continue
            pairs += 1
            paths = path_pair[src][dst]
            num_paths += len(paths)
            if len(paths) < minimum_number_of_path: minimum_number_of_path = len(paths)
            
            if len(paths) > 15:
                print(src, dst)
            s = 0
            for path in paths:
                s += len(path)
            sum_lengths += s / float(len(paths))
            # time.sleep(1)
            # print(num_paths)
            
    print("Switches: %d k: %d Average_path_length: %.2f Avg_num_of_paths: %.2f Avg_minimum_num_of_paths: %.2f" % (
        switch,
        k,
        sum_lengths / float(pairs),
        num_paths / float(pairs),
        minimum_number_of_path
    ))
    

if __name__ == "__main__":
    switches = [ 1000]
    s_map_ks = {
        500 : [4],
        1000 : [4],
        5000 : [4,5,6]
    }
    
    path_fmt = "path/disjoint_%d_%d"
    
    # for switch in switches:
    #     for k in s_map_ks[switch]:
    #         path_f = path_fmt % (switch, k)
    get_metric(500, 3)
    # get_metric(500, 4)
    # get_metric(500, 5)
    
    # get_metric(1000, 4)
    # get_metric(1000, 5)