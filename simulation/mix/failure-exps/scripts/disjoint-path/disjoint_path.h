#ifndef _CC_DISJOINT_PATH_H
#define _CC_DISJOINT_PATH_H

#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include <memory>
#include "gvar.h"




// compile pass
// g++ disjoint-updown-path.cc -std=c++11 -o disjoint -I /opt/gurobi951/linux64/include/ -L /opt/gurobi951/linux64/lib/ -lgurobi_c++ -lgurobi95 
// 
#define FOR_EACH(vec) {for (int l = 0; l < vec.size(); l++) { if (l == vec.size() - 1) { std::cout << vec[l];} else { std::cout << vec[l] << " "; } } std::cout << "\n";}
#define INF 1024
#define THREAD_NUM 32
typedef std::shared_ptr<std::vector<int> > OneDimVectorPtr;



int prepare_up_and_down_subgraph(
    std::vector<std::vector<int> >& up_mapped_edges, 
    std::vector<std::vector<int> >& down_mapped_edges,
    int vs_id
);

void prepare_subgraph_all();

namespace operations_research {

int edge_disjoint_updown_path(
    std::unordered_map<int, std::vector<std::vector<int> > >& vertex_map_edges,
    std::unordered_map<int, std::unordered_map<int, int> > &edge_flows,
    int source, 
    int destination
);

}// namespace operation_research

bool has_duplicate_edge(
    const std::vector<int>& compared_path, 
    const std::vector<int>& reference_path, 
    std::vector<std::vector<int> >& nonduplicate_edges,
    std::unordered_map<int, std::unordered_map<int, int> > &edge_flow
);

bool find_duplicate_edge(
    const std::vector<int>& compared_path, 
    const std::vector<int>& reference_path, 
    std::vector<std::vector<int> >& nonduplicate_edges,
    std::unordered_map<int, std::unordered_map<int, int> > &edge_flow
);

/**
 * @brief Used in get_disjoint_paths method
 */
int dfs(
    std::unordered_map<int, std::vector<std::vector<int> > >& vertex_map_edges,
    std::unordered_map<int, std::unordered_map<int, int> > &edge_flows,
    std::vector<std::vector<int> >& disjoint_paths,
    std::vector<int>& one_path,
    int source, int destination, int next_vertex
);

int get_disjoint_paths(std::vector<std::vector<int> >& disjoint_paths, int source, int destination);


/**
 * @brief Used in edge_disjoint_routing method for multi-thread calculation
 */
int thread_cal(std::vector<std::vector<int> > tasks, int thread_id);

int edge_disjoint_routing(const std::string& path_dir);

/**
 * @brief map port id to the virtual layer number
 */
int map_port_to_virtual_layers(int port_id);

/**
 * @brief map virtual switch id to real physical switch
 */
int map_vs_id_to_physical_switch(int vs_id);

/**
 * @brief map virtual switch id to the corresponding virtual layer
 */
int map_vs_id_to_virtual_layer(int vs_id);

/** 
 * @brief check if the edge is virtual
 */
int is_virtual_edge(int from, int to);

/**
 *  The mat file format should be organized as follows:
 *  to_hosts switches sw2sw_link_num num_of_servers
 *  num_virtual_layers ports_of_vir1 ports_of_vir2 ...
 *  s1  s2  p1   p2
 *  ...............
 *  s11 s12 p11 p12
 *  ...............
 */
void  construct_updown_directed_graph(const std::string& mat_file);


#endif