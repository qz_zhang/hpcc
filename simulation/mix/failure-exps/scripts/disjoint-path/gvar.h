
#ifndef _CC_GVAR_PATH_H
#define _CC_GVAR_PATH_H
#include <unordered_map>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <set>
// #define DEBUG
extern std::unordered_map<int, std::unordered_map<int, std::vector< std::vector<int > > > >  edge_disjoint_paths;

extern std::vector<std::vector<int> >  updown_directed_graph;

extern std::unordered_map<int,  std::vector<int> >  updown_graph;

extern std::vector<int> ports_of_virtual_layers;

extern std::mutex path_cal_mutex;
extern std::condition_variable finish_cal_cv;

extern uint32_t finished_task;
extern int switches, to_hosts, sw2sw_link_num, num_of_servers;
extern int num_virtual_layers;
extern std::unordered_map<int, std::vector<std::vector<int> > > up_subgraph;
extern std::unordered_map<int, std::vector<std::vector<int> > > down_subgraph;

void reset_variable();
#endif