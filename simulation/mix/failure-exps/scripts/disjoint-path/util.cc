#include "disjoint_path.h"

#include <algorithm>
#include <numeric>
#include <fstream>
#include "gvar.h"


int is_virtual_edge(int from, int to) {
    return map_vs_id_to_physical_switch(from) == map_vs_id_to_physical_switch(to);
}

int map_vs_id_to_virtual_layer(int vs_id) {
    int num_vs = (2 * num_virtual_layers - 1) * switches;
    // printf("switches: %d, num_virtual_layer: %d\n", switches, num_virtual_layers);
    // printf("num_vs: %d\n", num_vs);
    // std::unordered_map<int, std::vector<std::vector<int> > > vs_id_range_of_virtual_layer;
    for (int i = 0; i < num_virtual_layers; i++) {
        if (i != num_virtual_layers - 1) { // not the top layer
            //
            int vs_lower_id = i * 2 * switches;
            int vs_upper_id = (i + 1) * 2 * switches;
            if (vs_id >= vs_lower_id && vs_id < vs_upper_id) {
                return i;
            }
        } else {
            // vs id at the top layer
            return i;
        }
    }
    return -1;
}

int map_vs_id_to_physical_switch(int vs_id) {
    int num_vs = (2 * num_virtual_layers - 1) * switches;
    if (vs_id > num_vs || vs_id < 0) {
        return -1;
    }
    int mapped_virtual_layer = map_vs_id_to_virtual_layer(vs_id);
    // printf("mapped_virtual_layer: %d\n", mapped_virtual_layer);
    int phy_switch = -1;
    if (mapped_virtual_layer == num_virtual_layers - 1) {
        // top layer
        phy_switch = vs_id - 2 * mapped_virtual_layer  * switches;
    } else {
        phy_switch = ( vs_id - 2 * mapped_virtual_layer * switches ) / 2;
    }
    return phy_switch;
}



int map_port_to_virtual_layers(int port_id) {
    //
    int maximum_ports = std::accumulate(ports_of_virtual_layers.begin(), ports_of_virtual_layers.end(), to_hosts);
    // FOR_EACH(ports_of_virtual_layers);
    // std::cout << "maximum_ports: " << maximum_ports << " to_hosts: " << to_hosts << "\n";
    if (port_id < to_hosts || port_id >= maximum_ports) {
        return -1;
    }
    // to_hosts = 0;
    int cur = to_hosts;
    int prev = to_hosts;
    for (int i = 0; i < num_virtual_layers; i++) {
        cur += ports_of_virtual_layers[i];
        if (prev <= port_id &&  port_id < cur) {
            return i;
        }
        prev = cur;
    }
    return -1;
}

void construct_updown_directed_graph(const std::string& mat_file) {
    std::cout  << "Construct Updown Directed Graph\n";
    std::ifstream ifs;
    ifs.open(mat_file.c_str());
    // defined in gvar.h
    ifs >> to_hosts >> switches >> sw2sw_link_num >> num_of_servers;
    ifs >> num_virtual_layers;
    ports_of_virtual_layers.resize(num_virtual_layers, 0);
    for (int i = 0; i < num_virtual_layers; i++)   ifs >> ports_of_virtual_layers[i];
    printf("to_hosts: %d switches: %d s2w links: %d num_servers: %d\n", to_hosts, switches, sw2sw_link_num, num_of_servers);
    printf("num_virtual_layer: %d\n", num_virtual_layers);
    FOR_EACH(ports_of_virtual_layers);
    printf("num_virtual_layer: %d\n", num_virtual_layers);
    int num_vs_nodes = (2 * num_virtual_layers - 1) * switches;
    printf("num_vs_nodes: %d\n", num_vs_nodes);
    updown_directed_graph.resize(num_vs_nodes, std::vector<int>(num_vs_nodes, 0));
    
    /**
     * Initialize the virtual edges from the same physical switch
     */
    for (int s = 0; s < switches; s++ ) {
        for (int l = 0; l < num_virtual_layers - 1; l++) {
            if (l == num_virtual_layers - 2) { // TOP LAYER
                int top_vs_id = 2 * switches * (l + 1) + s;
                int lower_up_vs_id = 2 * s + 2 * l * switches;
                int lower_down_vs_id = (2 * s + 1) + 2 * l * switches;
                // printf("top s: %d top_vs: %d lower_up_vs: %d lower_down_vs: %d\n", s, top_vs_id, lower_up_vs_id, lower_down_vs_id);
                updown_directed_graph[lower_up_vs_id][top_vs_id] = 1;
                updown_directed_graph[top_vs_id][lower_down_vs_id] = 1;

                updown_graph[lower_up_vs_id].push_back(top_vs_id);
                updown_graph[top_vs_id].push_back(lower_down_vs_id);
            } else {
                int lower_up_vs_id = 2 * s + 2 * l * switches;
                int upper_up_vs_id = 2 * s + 2 * (l + 1) * switches;
                int lower_down_vs_id = (2 * s + 1) + 2 * l * switches;
                int upper_down_vs_id = (2 * s + 1) + 2 * (l + 1) * switches;
                
                updown_directed_graph[lower_up_vs_id][upper_up_vs_id] = 1;
                updown_directed_graph[upper_down_vs_id][lower_down_vs_id] = 1;

                // updown_graph[lower_up_vs_id].push_back(upper_up_vs_id);
                // updown_graph[upper_down_vs_id].push_back(lower_down_vs_id);
                // printf("Middle : %d  (%d, %d)=%d  (%d, %d)=%d\n", s, 
                //     lower_up_vs_id, 
                //     upper_up_vs_id, 
                //     updown_directed_graph[lower_up_vs_id][upper_up_vs_id], 
                //     lower_down_vs_id, 
                //     upper_down_vs_id,
                //     updown_directed_graph[upper_down_vs_id][lower_down_vs_id]);
            }
        }
    }

    
    for (int i = 0; i < sw2sw_link_num; i++) {
        int s1, s2, p1, p2;
        ifs >> s1 >> s2 >> p1 >> p2;
        int l1 = map_port_to_virtual_layers(p1), l2 = map_port_to_virtual_layers(p2);
        // printf("s1: %d s2: %d p1: %d p2: %d l1: %d l2: %d\n", s1, s2, p1, p2, l1, l2);
        if (l1 == num_virtual_layers - 1 || l2 == num_virtual_layers - 1) {
            int top_vs_id = -1, lower_layer_up_vs_id = -1, lower_layer_down_vs_id = -1;
            if (l1 == num_virtual_layers - 1) {
                // l1 is the top layer
                top_vs_id = s1 + 2 * l1 * switches;
                lower_layer_up_vs_id = 2 * s2 + 2 * l2 * switches;
                lower_layer_down_vs_id = 2 * s2 + 1 + 2 * l2 * switches;
            } else {
                // l2 is the top layer
                top_vs_id = s2 + 2 * l2 * switches;
                lower_layer_up_vs_id = 2 * s1 + 2 * l1 * switches;
                lower_layer_down_vs_id = 2 * s1 + 1 + 2 * l1 * switches;
            }

            updown_directed_graph[lower_layer_up_vs_id][top_vs_id] = 1;
            updown_directed_graph[top_vs_id][lower_layer_down_vs_id] = 1;

            
            // updown_graph[lower_layer_up_vs_id].push_back(top_vs_id);
            // updown_graph[top_vs_id].push_back(lower_layer_down_vs_id);
            // printf("top_vs: %d lower_up_vs: %d lower_down_vs: %d\n", top_vs_id, lower_layer_up_vs_id, lower_layer_down_vs_id);
        } else { // l1 and l2 are middle layers.
            int up_vs_id1, up_vs_id2, down_vs_id1, down_vs_id2;
            up_vs_id1 = (2 * s1) + 2 * l1 * switches;
            down_vs_id1 = (2 * s1 + 1) + 2 * l1 * switches;
            up_vs_id2 = (2 * s2) + 2 * l2 * switches;
            down_vs_id2 = (2 * s2 + 1) + 2 * l2 * switches;
            // std::cout << "middle layer: " << l1 << ", " << l2 << "\n";
            // std::cout << "middle layer " << up_vs_id1 << ", " << down_vs_id1 << ", " << up_vs_id2 << ", " << down_vs_id2 << "\n";
            if (l1 < l2) { // The l2-layer is the upper layer

                updown_directed_graph[up_vs_id1][up_vs_id2] = 1;
                updown_directed_graph[down_vs_id2][down_vs_id1] = 1;
                
                // updown_graph[up_vs_id1].push_back(up_vs_id2);
                // updown_graph[down_vs_id2].push_back(down_vs_id1);
            } else if (l1 > l2) {
                updown_directed_graph[up_vs_id2][up_vs_id1] = 1;
                updown_directed_graph[down_vs_id1][down_vs_id2] = 1;

                // updown_graph[up_vs_id2].push_back(up_vs_id1);
                // updown_graph[down_vs_id1].push_back(down_vs_id2);
            }
        }
    }


    for (int i = 0; i < num_vs_nodes; i++) {
        for (int j = 0; j < num_vs_nodes; j++) {
            if (updown_directed_graph[i][j] == 1) {
                updown_graph[i].push_back(j);
            }
        }
    }
    // printf("after initializing the edges and display the updown_graph\n");
    // for (int i = 0; i < num_vs_nodes; i++) {
    //     printf("vnode: %d traverse: ", i);
    //     FOR_EACH(updown_graph[i]);
    // }
    // printf("updown_graph[0]: %d\n", updown_graph[0].size());
    
    // std::cout << "close\n";
    // printf("updown_directed_graph size: %d %d\n", updown_directed_graph.size(), updown_directed_graph[0].size());
    ifs.close();
}

