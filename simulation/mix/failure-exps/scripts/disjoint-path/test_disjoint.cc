#include "disjoint_path.h"
#include "gvar.h"


void init_global_static_vars() {
    ports_of_virtual_layers = {1,2,1};
    switches = 5;
    to_hosts = 1;
    sw2sw_link_num = 20;
    num_of_servers = 5;
    num_virtual_layers = ports_of_virtual_layers.size();
}

void test_map_port_to_layers() { 
    num_virtual_layers = 3;
    ports_of_virtual_layers = {1,2,1};
    to_hosts = 2;

    printf("Port_id:[%d] Mapped Layers:[%d]\n", 1, map_port_to_virtual_layers(1));
    printf("Port_id:[%d] Mapped Layers:[%d]\n", 2, map_port_to_virtual_layers(2));
    printf("Port_id:[%d] Mapped Layers:[%d]\n", 3, map_port_to_virtual_layers(3));
    printf("Port_id:[%d] Mapped Layers:[%d]\n", 4, map_port_to_virtual_layers(4));
    printf("Port_id:[%d] Mapped Layers:[%d]\n", 5, map_port_to_virtual_layers(5));
    printf("Port_id:[%d] Mapped Layers:[%d]\n", 6, map_port_to_virtual_layers(6));
    printf("Port_id:[%d] Mapped Layers:[%d]\n", 8, map_port_to_virtual_layers(8));
}

void test_map_vs_id_to_layer() {
    init_global_static_vars();
    printf("switches: %d, num_virtual_layer: %d\n", switches, num_virtual_layers);
    FOR_EACH(ports_of_virtual_layers);
    int vs_id = 4;
    printf("Vs_id:[%d] Mapped Virtual Layer:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
    vs_id = 9;
    printf("Vs_id:[%d] Mapped Virtual Layer:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
    vs_id = 10;
    printf("Vs_id:[%d] Mapped Virtual Layer:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
    vs_id = 17;
    printf("Vs_id:[%d] Mapped Virtual Layer:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
    vs_id = 22;
    printf("Vs_id:[%d] Mapped Virtual Layer:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
    vs_id = 24;
    printf("Vs_id:[%d] Mapped Virtual Layer:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
}

void test_map_function() {
    //
    init_global_static_vars();
    printf("switches: %d, num_virtual_layer: %d\n", switches, num_virtual_layers);
    FOR_EACH(ports_of_virtual_layers);
    int vs_id = 4;
    printf("Vs_id:[%d] Mapped Physical Switch:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
    vs_id = 9;
    printf("Vs_id:[%d] Mapped Physical Switch:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
    vs_id = 14;
    printf("Vs_id:[%d] Mapped Physical Switch:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
    vs_id = 24;
    printf("Vs_id:[%d] Mapped Physical Switch:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
    vs_id = 22;
    printf("Vs_id:[%d] Mapped Physical Switch:[%d]\n", vs_id, map_vs_id_to_virtual_layer(vs_id));
}

void test_dfs_find_path() {
    std::vector<int> compared_path = {810,703,293,37};
    std::vector<int> reference_path = {819,703,293,37};
    std::vector<std::vector<int > > nonduplicated_edges;
    std::unordered_map<int, std::unordered_map<int, int> > edge_flows;
    edge_flows[703][293] = 0;
    
    bool ret = has_duplicate_edge(compared_path, reference_path, nonduplicated_edges, edge_flows);
    // for (auto &non_e : nonduplicated_edges) {
    //     FOR_EACH(non_e);
    // }
    if (ret) {
        printf("has duplicated edges\n");
    }
}

void test_construct_updown_directed_graph(std::string topo_file, std::string path_ofile) {

    construct_updown_directed_graph(topo_file);
    prepare_subgraph_all();

#ifdef DEBUG
    int special_source = 7, speical_dest = 434;
    std::vector<std::vector<int> > disjoint_path;
    int ret = get_disjoint_paths(disjoint_path, special_source, speical_dest);
    for (auto path : disjoint_path) {
        FOR_EACH(path);
    }
    printf("ret = %d real paths: %d\n", ret, disjoint_path.size());
#else
    edge_disjoint_routing(path_ofile);
    // std::vector<std::vector<int> > disjoint_path;
    // int ret = get_disjoint_paths(disjoint_path, 0, 1);
    // for (auto &path : disjoint_path) {
    //     FOR_EACH(path);
    // }
    // clock_t start, end;
    // start = clock();
    // int sum = 0;
    // int pair = 0;
    // for (int i = 0; i < 10; i++) {
    //     for (int j = i + 1; j < switches; j++) {
    //         std::vector<std::vector<int> > disjoint_path;
    //         int ret = get_disjoint_paths(disjoint_path, i, j);
    //         sum += ret;
    //         // if (ret == 0) {
    //         //     printf("%d -> %d ret=0\n", i, j);
    //         // }
    //         pair += 1;
    //     }
    // }
    // end = clock();
    // printf("Avg: edge disjoint paths: %.2f running: %ds\n", (double)sum / pair, (end - start) / CLOCKS_PER_SEC);
    
#endif
}


// nohup stdbuf -oL ./disjoint >  log & 
// #define RUN_DISJOINT_TEST

int main(int argc, char **argv) {
    // test_construct_updown_directed_graph();
    if (argc < 3) {
        std::cerr << "USAGE: ./disjoint [topo-dir] [path_out_dir]\n";
        exit(-1);
    }
    std::string topo_dir(argv[1]);
    std::string path_out_dir(argv[2]);
    printf("TOPOLOGY_FILE: %s PATH_OUTPUT_FILE: %s\n", topo_dir.c_str(), path_out_dir.c_str());
    // test_construct_updown_directed_graph(topo_dir, path_out_dir);
    
    // construct_updown_directed_graph(topo_dir);
    // edge_disjoint_routing(path_out_dir);
    // test_map_vs_id_to_layer();
    // test_disjoint_paths();
    // test_dfs_find_path();
    // printf("500\n");
#ifdef RUN_DISJOINT_TEST
    printf("Not regular 500\n");
    reset_variable();
    test_construct_updown_directed_graph("topo/fc_500_4", "path/disjoint_500_4");
    printf("\nNot regular 1000\n");
    reset_variable();
    test_construct_updown_directed_graph("topo/fc_1000_4", "path/disjoint_1000_4");
    printf("\nNot regular 2000\n");
    reset_variable();
    test_construct_updown_directed_graph("topo/fc_2000_4", "path/disjoint_2000_4");
    printf("\nNot regular 3000\n");
    reset_variable();
    test_construct_updown_directed_graph("topo/fc_3000_4", "path/disjoint_3000_4");
    printf("\nNot regular 5000\n");
    reset_variable();
    test_construct_updown_directed_graph("topo/fc_5000_5", "path/disjoint_5000_5");
#else
    test_construct_updown_directed_graph(topo_dir, path_out_dir);
#endif
    return 0;
}

