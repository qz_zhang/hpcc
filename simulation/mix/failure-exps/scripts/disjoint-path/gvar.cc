#include "gvar.h"

std::unordered_map<int, std::unordered_map<int, std::vector< std::vector<int > > > >  edge_disjoint_paths;

std::vector<std::vector<int> >  updown_directed_graph;

std::unordered_map<int,  std::vector<int> >  updown_graph;

std::unordered_map<int, std::vector<std::vector<int> > >  up_subgraph;
std::unordered_map<int, std::vector<std::vector<int> > >  down_subgraph;

std::vector<int> ports_of_virtual_layers;

std::mutex path_cal_mutex;
std::condition_variable finish_cal_cv;

uint32_t finished_task;

int switches, to_hosts, sw2sw_link_num, num_of_servers;

int num_virtual_layers;

void reset_variable() {
    edge_disjoint_paths.clear();
    updown_directed_graph.clear();
    updown_graph.clear();
    up_subgraph.clear();
    down_subgraph.clear();
    ports_of_virtual_layers.clear();
    finished_task = 0;
    switches = 0;
    to_hosts = 0;
    sw2sw_link_num = 0;
    num_of_servers = 0;
    num_virtual_layers = 0;
}