#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include <memory>
#include <numeric>
#include <sstream>
#include <cstdint>
#include <thread>
#include <algorithm>
#include <queue>
#include <climits>
#include <fstream>
#include "disjoint_path.h"
#include "ortools/graph/min_cost_flow.h"
#include <unistd.h>
#include <assert.h>


int prepare_up_and_down_subgraph(
    std::vector<std::vector<int> >& up_mapped_edges, 
    std::vector<std::vector<int> >& down_mapped_edges, 
    int up_vs_id
) {
    // calculate the upsubgraph and get the downsubgraph of (up_vs_id + 1) by this upsubgraph
    up_mapped_edges.clear();
    down_mapped_edges.clear();
    std::queue<int> q;

    std::vector<std::vector<int> > vs_up_mapped_edges;
    std::unordered_map<int, std::unordered_map<int, int> > up_visited;
    // std::unordered_map<int, std::unordered_map<int, int> > down_visited;
    q.push(up_vs_id);
    while (!q.empty())
    {   
        // std::cout << "enter while\n";
        int u = q.front();
        q.pop();
        for (auto v : updown_graph[u]/* int v = 0; v < updown_directed_graph[u].size(); v++  */) {
            if (v > u && !up_visited[u][v]) {
                if (v >= 2 * (num_virtual_layers - 1)  * switches && v < (2 * num_virtual_layers - 1) * switches) {
                    vs_up_mapped_edges.push_back({u,v, 1}); // u is the node at the top layer
                } else {
                    vs_up_mapped_edges.push_back({u,v});
                }
                up_mapped_edges.push_back({u,v});
                up_visited[u][v] = 1;
                q.push(v);
            }
        }
    }
    

    // DONE: convert up links from destination to down links to destination
    for (auto &e : vs_up_mapped_edges) {
        // destination_mapped_up_edges.push_back()
        if (e.size() == 3) {
            down_mapped_edges.push_back({e[1], e[0] + 1});

        } else {
            down_mapped_edges.push_back({e[1] + 1, e[0] + 1});
        }
    }
    // printf("vertext set size: %d\n", vertex_set.size());
    return 0;
}

void prepare_subgraph_all() {
    clock_t start, end;
    start = clock();
    printf("Start prepare...\n");
    std::vector<std::vector<int> > up_mapped_edges;
    std::vector<std::vector<int> > down_mapped_edges;
    for (int i = 0; i < switches; i++) {
        up_mapped_edges.clear();
        down_mapped_edges.clear();
        prepare_up_and_down_subgraph(up_mapped_edges, down_mapped_edges, 2 * i);
        for (auto& up_edge : up_mapped_edges) {
            // FOR_EACH(up_edge);
            up_subgraph[2 * i].push_back(up_edge);
        }
        for (auto& down_edge : down_mapped_edges) {
            down_subgraph[2 * i + 1].push_back(down_edge);
        }
    }
    end = clock();
    printf("Prepare stage: %ds\n", (end - start) / CLOCKS_PER_SEC);
}


namespace operations_research {
int edge_disjoint_updown_path(
    std::unordered_map<int, std::vector<std::vector<int> > >& vertex_map_edges,
    std::unordered_map<int, std::unordered_map<int, int> > &edge_flows, 
    int source, int destination
) {
    //
    int source_up_vs_id = 2 * source, dest_down_vs_id = 2 * destination + 1;
    SimpleMinCostFlow min_cost_flow;
    
    // printf("source_up: %d dest_down: %d\n", source_up_vs_id, dest_down_vs_id);
    min_cost_flow.AddArcWithCapacityAndUnitCost(dest_down_vs_id, source_up_vs_id, INT32_MAX, -0xfffff);
    for (auto& e : up_subgraph[source_up_vs_id]) {
        // if (e[0] == 2516 && e[1] == 3008) continue;
        // printf("up mapped edge: %d->%d\n", e[0], e[1]);
        if (is_virtual_edge(e[0], e[1])) {
            // virtual switches belonging to one physical switch
            //  printf("up virtual edge: (%d %d)\n", e[0], e[1]);
            min_cost_flow.AddArcWithCapacityAndUnitCost(e[0], e[1], INT32_MAX, 0);
        } else {
            min_cost_flow.AddArcWithCapacityAndUnitCost(e[0], e[1], 1, 1);
        }
    }
    

    for (auto& e : down_subgraph[dest_down_vs_id]) {
        // printf("down mapped edge: %d->%d\n", e[0], e[1]);
        if (is_virtual_edge(e[0], e[1])) {
            // virtual switches belonging to one physical switch
            //  printf("down virtual edge: (%d %d)\n", e[0], e[1]);
            min_cost_flow.AddArcWithCapacityAndUnitCost(e[0], e[1], INT32_MAX, 0);
        } else {
            min_cost_flow.AddArcWithCapacityAndUnitCost(e[0], e[1], 1, 1);
        }
    }

    // printf("vertex: %d mapped edges: %d\n", 2516, vertex_map_edges[2516].size());
    int k_edge_disjoint_paths = 0;
    int status = min_cost_flow.Solve();
    if (status == MinCostFlow::OPTIMAL) {

        for (std::size_t i = 0; i < min_cost_flow.NumArcs(); ++i) {
            int64_t cost = min_cost_flow.Flow(i) * min_cost_flow.UnitCost(i);
            if (min_cost_flow.Flow(i) > 0) {
                if (min_cost_flow.Tail(i) == dest_down_vs_id && min_cost_flow.Head(i) == source_up_vs_id) {
                    k_edge_disjoint_paths = min_cost_flow.Flow(i);
                }
                edge_flows[ min_cost_flow.Tail(i) ][ min_cost_flow.Head(i) ] =  min_cost_flow.Flow(i);
                
                #ifdef DEBUG
                printf("%d-> %d flow: %d\n", min_cost_flow.Tail(i),  min_cost_flow.Head(i), min_cost_flow.Flow(i));
                #endif

                vertex_map_edges[min_cost_flow.Tail(i)].push_back({ min_cost_flow.Tail(i), min_cost_flow.Head(i)});
            }
        }
    }

    // printf("vertex: %d mapped edges: %d\n", 2516, vertex_map_edges[2516].size());
    return k_edge_disjoint_paths;
}

} // namespace operation_research


bool has_duplicate_edge(
    const std::vector<int>& compared_path, 
    const std::vector<int>& reference_path, 
    std::vector<std::vector<int> >& nonduplicate_edges,
    std::unordered_map<int, std::unordered_map<int, int> > &edge_flow
) {
    for (int i = 0; i < compared_path.size() - 1; i++) {
        for (int j = 0; j < reference_path.size() - 1; j++) {
            if ( (compared_path[i] == reference_path[j] 
                        && compared_path[i + 1] == reference_path[j + 1]) 
                        && edge_flow[ compared_path[i] ][ compared_path[i + 1] ] == 0 ) {
                return  true;
            }
        }
    }
    return false;
}

bool find_duplicate_edge(
    const std::vector<int>& compared_path, 
    const std::vector<int>& reference_path, 
    std::vector<std::vector<int> >& nonduplicate_edges,
    std::unordered_map<int, std::unordered_map<int, int> > &edge_flow
) {
    return true;
}


int dfs(std::unordered_map<int, std::vector<std::vector<int> > >& vertex_map_edges,
    std::unordered_map<int, std::unordered_map<int, int> > &edge_flow,
    std::vector<std::vector<int> >& disjoint_paths,
    std::vector<int>& one_path,
    int source, int destination, int next_vertex) {
        
    if (next_vertex == destination) {
        disjoint_paths.push_back(one_path);
        return 0;
    }

    for (auto  edge : vertex_map_edges[next_vertex]) {
        one_path.push_back(edge[1]);
        dfs(vertex_map_edges, edge_flow, disjoint_paths, one_path, source, destination, edge[1]);
        one_path.pop_back();
    }
}

bool check_is_valid_path(
    const std::vector<int>& path,
    std::unordered_map<int, std::unordered_map<int, int> > &edge_flow
) {
    bool valid = true;
    for (int i = 0; i < path.size() - 1; i++) {
        if (edge_flow[ path[i] ][ path[i + 1] ] == 0) {
            valid = false;
            break;
        }
    }
    return valid;
}

int get_disjoint_paths(
    std::vector<std::vector<int> >& disjoint_paths, 
    int source, int destination
) {
    std::unordered_map<int, std::vector<std::vector<int> > > vertex_map_edges;
    std::unordered_map<int, std::unordered_map<int, int> > edge_flows;
    int k_paths = operations_research::edge_disjoint_updown_path(vertex_map_edges, edge_flows, source, destination);
    std::vector<std::vector<int> > dfs_paths;
    std::vector<int> one_path;
    //
    int source_up_vs_id = 2 * source, dest_down_vs_id = 2 * destination + 1;
    one_path.push_back(source_up_vs_id);
    // printf("source up vs: %d, dest down: %d\n", source_up_vs_id, dest_down_vs_id);
    dfs(vertex_map_edges, edge_flows, dfs_paths, one_path, source_up_vs_id, dest_down_vs_id, source_up_vs_id);

    std::set<int> vertexes;
#ifdef DEBUG
    printf("------------> vpath <------------\n");
    for (auto path : dfs_paths) {
        FOR_EACH(path);
    }
    printf("------------> vpath <------------\n\n");
#endif
    for (int i = 0; i < dfs_paths.size(); i++) {
        if (check_is_valid_path(dfs_paths[i], edge_flows)) {
            #ifdef DEBUG
            // printf("<---------------  Round Begin  --------------->\n");
            FOR_EACH(dfs_paths[i]);
            #endif
            for (int j = 0; j < dfs_paths[i].size() - 1; j++) {
                // printf("edee: %d %d\n", dfs_paths[i][j],  dfs_paths[i][j + 1]);
                edge_flows[ dfs_paths[i][j] ][ dfs_paths[i][j + 1] ] -= 1;
            }
            std::vector<int> phy_path;
        
            for (int j = 0; j < dfs_paths[i].size(); j++) {
                vertexes.insert(dfs_paths[i][j]);
                int mapped_phy_id =  map_vs_id_to_physical_switch(dfs_paths[i][j]);
                if (phy_path.size() == 0 || phy_path[phy_path.size() - 1] != mapped_phy_id) {
                    phy_path.push_back(mapped_phy_id);
                }
            }
            #ifdef DEBUG
            FOR_EACH(phy_path);
            // printf("<--------------- Round End  --------------->\n");
            #endif
            disjoint_paths.push_back(phy_path);
        }
    }
    // printf("0 -> 256: %d edge flow\n", edge_flows[0][256]);
    // printf("256 -> 512: %d edge flow\n", edge_flows[256][512]);
    if (k_paths != disjoint_paths.size() ) {
        printf("ERR !!! %d->%d k_paths: %d disjoint_paths.size: %d\n", source, destination, k_paths, disjoint_paths.size());
        // for (auto path : disjoint_paths) {
        //     FOR_EACH(path);
        // }
        #ifdef DEBUG
        for (auto v : vertexes) {
            printf("vnode: %d phy_id: %d\n", v, map_vs_id_to_physical_switch(v));
        }
        for (auto pair : vertex_map_edges) {
            printf("v:%d {", pair.first);
            for (auto &e: pair.second) {
                printf("%d ", e[1]);
            }
            printf("}\n");
        } 
        #endif
        exit(-1);
        // assert(k_paths == disjoint_paths.size() && "k_paths is not equal to disjoint_paths.size()");
    }
    return k_paths;
}


int thread_cal(std::vector<std::vector<int> > tasks, int thread_id) {
    // 
    // printf("thread %d start, tasks:[%d]\n", thread_id, tasks.size());
    for (auto &task : tasks) {
        int source = task[0], dest = task[1];
        std::vector<std::vector<int> > disjoint_paths;
        get_disjoint_paths(disjoint_paths,source, dest);
        // 
        std::unique_lock<std::mutex> lck(path_cal_mutex);
        finished_task += 1;
        edge_disjoint_paths[source][dest] = disjoint_paths;
        // std::cout << finished_task << "\n";
        finish_cal_cv.notify_one();
    }
    // printf("thread:[%d] ended, finished:[%d]\n", thread_id, finished_task);
}

std::string VecToString(const std::vector<int>& vec) {
    std::string res = "";
    for (int i = 0; i < vec.size(); i++) {
        res += std::to_string(vec[i]);
        if (i < vec.size() - 1) res += " ";
    }
    return res;
}

int edge_disjoint_routing(const std::string& path_dir) {
    
    std::vector<std::vector<int> > tasks;
    printf("edge disjoint: %d\n", switches);
    for (int i = 0; i < switches; i++) {
        for (int j = i + 1; j < switches; j++) {
            tasks.push_back({i, j});
        }

    }
    //
    std::unordered_map<int, std::vector<std::vector<int> > > split_tasks;
    std::vector<int> ids {0};
    int total_task = tasks.size();
    for (int i = 1; i <= THREAD_NUM - 1; i++) {
        ids.push_back(i * (total_task / THREAD_NUM));
    }
    if (ids[ids.size() - 1] < total_task) ids.push_back(total_task);
    

    for (int i = 0; i < THREAD_NUM; i++) {
        std::vector<std::vector<int> > thread_i_task;
        for (int j = ids[i]; j < ids[i+1]; j++) {
            thread_i_task.push_back(tasks[j]);  
        }
        split_tasks[i] = thread_i_task;
        // printf("tasks:[%d]\n", split_tasks[i].size());
    }
    
    std::vector<std::unique_ptr< std::thread > > threads;
    for (int i = 0; i < THREAD_NUM; i++) {
        threads.push_back(std::unique_ptr< std::thread >(new std::thread(thread_cal, split_tasks[i], i ) ) );
    }

    // bind thread to CPU
    for (int i = 0; i < threads.size(); i++) {
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        CPU_SET(i, &cpuset);
        pthread_setaffinity_np(threads[i]->native_handle(), sizeof(cpuset), &cpuset);
    }

    int task_count = tasks.size();
    printf("tasks:[%d]\n", task_count);
    std::unique_lock<std::mutex> lck(path_cal_mutex);
    while (finished_task != task_count) {
        finish_cal_cv.wait(lck);
    }
    for (const auto &thread : threads) {
        thread->join();
    }

    std::cout << "Reverse Paths...\n";
    for (int i = 0; i < switches; i++) {
        for (int j = 0; j < switches; j++) {
            if (j >= i) continue;
            std::vector<std::vector<int> > i_to_j_paths; 
            for (auto p : edge_disjoint_paths[j][i]) {
                
                std::reverse(p.begin(), p.end());
                i_to_j_paths.push_back(p);
            }
            edge_disjoint_paths[i][j] = i_to_j_paths;
        }
    }

    double pairs = 0, avg_path_len = 0, avg_num_paths = 0, minimum_num_of_paths = 0xffffff;
    for (int i = 0; i < switches; i++) {
        for (int j = 0; j < switches; j++) {
            if (i == j) continue;
            pairs += 1;
            auto& paths = edge_disjoint_paths[i][j];
            avg_num_paths += paths.size();
            if (paths.size() < minimum_num_of_paths) minimum_num_of_paths = paths.size();
            double s = 0;
            for (auto &path : paths) {
                s += path.size();
            }

            avg_path_len += (s) / paths.size();

        }
    }

    printf("avg_num_of_paths: %.2f avg_path_len: %.2f minimum_num_of_paths: %.2f\n", 
        avg_num_paths / pairs,
        avg_path_len / pairs,
        minimum_num_of_paths);



    std::ofstream ofs(path_dir);
  
    char strBuf[256];

    sprintf(strBuf, "%d %d %d\n", switches * (switches - 1), to_hosts, switches);
    ofs << strBuf;

    for (int i = 0; i < switches; i++) {
        for (int j = 0; j < switches; j++) {
            if (i == j) continue;
            sprintf(strBuf, "%d %d %d\n", i, j, edge_disjoint_paths[i][j].size());
            ofs << strBuf;

            for (const auto &path : edge_disjoint_paths[i][j]) {
                sprintf(strBuf, "%d %s\n", path.size(), VecToString(path).c_str());
                ofs << strBuf;
            }
        }
    }
    std::cout << "Calculate over\n";
}
