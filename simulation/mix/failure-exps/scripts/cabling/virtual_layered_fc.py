from os import remove
from sys import stderr
import numpy as np
import math
import random
import time

class Switch:
  def __init__(self, id, ports=16, to_hosts=4, layers=3, ports_of_vir_layer=[4,6,2]):
    self.id = id
    self.used = [0 for _ in range(ports)]
    self.to_hosts = to_hosts
    # check param setting
    if layers != len(ports_of_vir_layer) or np.sum(ports_of_vir_layer) + to_hosts != ports:
      print("param error")
      exit(-1)
    self.layers = layers
    self.ports_of_vir_layer = ports_of_vir_layer
    self.port_ids = {}
    pre = 0
    for i in range(layers):
      self.port_ids[i] = [to_hosts + pre + ii for ii in range(ports_of_vir_layer[i])]
      pre = np.sum(ports_of_vir_layer[:i+1])

    # print(self.port_ids)

    ## FC has layer - 1 bipartite graphs
    self.degrees = []
    for i in range(layers):
      degree = 0
      if i == 0:
        degree = ports_of_vir_layer[0]
      else:
        degree = ports_of_vir_layer[i] - self.degrees[len(self.degrees) - 1]
      self.degrees.append(degree)


  def map_port_to_virtual_layers(self, port_id):
    for layer in range(self.layers):
      if port_id in self.port_ids[layer]:
        return layer
    return -1


  def get_vir_layer_i_ports(self, layer_i):
    return self.ports_of_vir_layer[layer_i]

  def mark_used(self, port_id):
    self.used[port_id] = 1
  
  def mark_unused(self, port_id):
    self.used[port_id] = 0

  def get_layer_i_unoccupied_port(self, layer):
    selected_port_no = -1
    for port_id in self.port_ids[layer]:
      if not self.used[port_id]:
        selected_port_no = port_id
    # print(selected_port_no)
    return selected_port_no

  def print_port_usage_of_layer(self, layer):
    for port_id in self.port_ids[layer]:
      if self.used[port_id]:
        print("Used  ! switch: {} port_id: {} layer: {}".format(self.id, port_id, layer))
      else:
        print("Unused! switch: {} port_id: {} layer: {}".format(self.id, port_id, layer))

def find_ele_in_list(list, ele):
  index = -1
  for i in range(len(list)):
    if list[i] == ele:
      index = i
  return index

class OCS:
  def __init__(self, ocs_id, ocs_tx_rx_pairs, group):
    self.ocs_id = ocs_id
    self.ocs_tx_rx_pairs = ocs_tx_rx_pairs
    self.group_i = group
    self.occupied_ports = 0
    self.ocs_connected_switches = {}
    ## 0 represents the lower ports in the layer_th group.
    for i in range(2):
      self.ocs_connected_switches[i] = []
  
  def add_switch(self, switch, is_lower):
    if is_lower: 
      self.ocs_connected_switches[0].append(switch)
      self.occupied_ports += 1
    else: self.ocs_connected_switches[1].append(switch)
  
  def can_add(self, tried_switch_id, is_lower):
    # time.sleep(1)
    # print('is full=', self.is_full())
    if len(self.ocs_connected_switches[0]) + len(self.ocs_connected_switches[1]) >= 2 * self.ocs_tx_rx_pairs :
      return False
    can = True
    if is_lower:
      index = find_ele_in_list(self.ocs_connected_switches[0], tried_switch_id)
      if index != -1: can = False
    else:
      index = find_ele_in_list(self.ocs_connected_switches[1], tried_switch_id)
      if index != -1: can = False
    # print('can=', self.is_full())
    return can

  def print_ToR(self):
    ToR_str = "OCS: %d GROUP_%d OCCUPIED_PORRS: %d\nLOWER TORs: %s\nUPPER TORs: %s\n" % (self.ocs_id, self.group_i, self.occupied_ports,
                                                " ".join(map(str, self.ocs_connected_switches[0])), 
                                                " ".join(map(str, self.ocs_connected_switches[1])))
    print(ToR_str)
    
  def __str__(self) -> str:
    return "OCS id: %d belonging layer: %d" % (self.ocs_id, self.group_i)

def n_ocs_for_layer_i(switches, ocs_tx_rx_pairs, layer_degree):
  return math.ceil(switches * 2 * (layer_degree) / ocs_tx_rx_pairs)

def evenly_distribute_ToR(switches, layer, layer_degree, OCS_objs):
  if switches % 2 != 0:
    print("The number of switches must be even!")
    exit(-1)
  
  n_ocs = len(OCS_objs[layer])
  
  switch_list = [switch for switch in range(switches)]
  while len(switch_list) > 0:
    choosed_switch = random.choice(switch_list)
    switch_list.remove(choosed_switch)
    ocs_cache = []
    
    # print("choose switch = ",choosed_switch)
    # print("switch list = ",len(switch_list))
    # ## FOR EACH SWITCH
    ## We must make sure that lower == upper
    for p in range(2 * layer_degree):
      selected_ocs = random.randint(0, n_ocs - 1)
      is_lower = True
      if p >= layer_degree: is_lower = False
      while not OCS_objs[layer][selected_ocs].can_add(choosed_switch, is_lower):
        selected_ocs = random.randint(0, n_ocs - 1)
      # print("selected OCS=", selected_ocs)
      ocs_cache.append( (selected_ocs, is_lower ) )
      OCS_objs[layer][selected_ocs].add_switch(choosed_switch, is_lower)
    
    choosed_switch2 = random.choice(switch_list)
    switch_list.remove(choosed_switch2)
    for ocs_pair in ocs_cache:
      OCS_objs[layer][ocs_pair[0]].add_switch(choosed_switch2, not ocs_pair[1])

def redistribute_ToR():
  pass

def make_ToR_to_TOR_conncection(layer, OCS_objs, switch_objs, topo_matrix, ports_conn_matrix):
  # print("Make ToR to TOR conncection layer-%d" % (layer))
  for ocs in OCS_objs[layer]:
    upper_switches = ocs.ocs_connected_switches[0]
    lower_switches = ocs.ocs_connected_switches[1]
    while len(lower_switches) >0 :
      s1 = random.choice(lower_switches)
      s2 = random.choice(upper_switches)
      
      rechoice_count = 0
      while s1 == s2 or topo_matrix[s1][s2] == 1 or topo_matrix[s2][s1]:
        s2 = random.choice(upper_switches)
        rechoice_count += 1
        if rechoice_count > 2 * (len(upper_switches)):
          # print("error in make connection")
          return False
      p1 = switch_objs[s1].get_layer_i_unoccupied_port(layer)
      p2 = switch_objs[s2].get_layer_i_unoccupied_port(layer + 1)
      
      topo_matrix[s1][s2] = 1
      topo_matrix[s2][s1] = 1
      ports_conn_matrix[s1][s2] = [p1, p2]
      ports_conn_matrix[s2][s1] = [p2, p1]
      # print("%d: (%d->%d) (%d->%d)" % (layer, s1, s2, s2, s1))
      ## Remove
      lower_switches.remove(s1)
      upper_switches.remove(s2)
  return True

def virtual_layered_fc(switches, to_hosts, ports, ports_of_virtual_layer, ocs_tx_rx_pairs=320, mat_dir=None):
    if to_hosts + np.sum(ports_of_virtual_layer) != ports:
        print(stderr, "error in params")
        exit(-1)
    layers = len(ports_of_virtual_layer)
    
    ## degrees
    degrees = []
    degrees.append(ports_of_virtual_layer[0])
    for i in range(1, layers - 1):
        last_idx = len(degrees) - 1
        degrees.append(ports_of_virtual_layer[i] - degrees[last_idx])
    # print(degrees)
    
    ocs_for_each_layer = []
    for i in range(layers - 1):
      ocs_for_each_layer.append( n_ocs_for_layer_i(switches, ocs_tx_rx_pairs, degrees[i] ) )
    # print(ocs_for_each_layer)
    
    ##create switch objects
    switch_objs = []
    for i in range(switches):
        switch_objs.append(Switch(i, ports, to_hosts, layers, ports_of_virtual_layer))
    
    ##create OCS objects
    OCS_objs = {}
    for layer in range(layers - 1):
      OCS_objs[layer] = []
      for i in range(ocs_for_each_layer[layer]):
        OCS_objs[layer].append(OCS(i, ocs_tx_rx_pairs, layer))
    
    topo_matrix = [[0 for _ in range(switches)] for _ in range(switches) ]
    ports_conn_matrix = {}
    for i in range(switches):
        ports_conn_matrix[i] = {}
        for j in range(switches):
            ports_conn_matrix[i][j] = []
    
    ## evenly distribute ToR switches to OCSs of each group
    for i in range(layers - 1):
      # print("Distrute ToR for layer-%d" % (i))
      evenly_distribute_ToR(switches, i, degrees[i], OCS_objs)
      # print("Make ToR-to-ToR connection for layer-%d" % (i))
      ret = make_ToR_to_TOR_conncection(i, OCS_objs, switch_objs, topo_matrix, ports_conn_matrix)
      # if not ret:
      #   print("reditribute ToR")
        
      #   print("remake connection") 
    # connect ToR switch to OCS, each OCS group has n_ocs_per_layer OCSs.
    # for i in range(layers - 1):
    #   for ocs in OCS_objs[i]:
    #     ocs.print_ToR()
    
    return topo_matrix, ports_conn_matrix
    # print('ideal links= %d real links = %d' % (switches * (ports - to_hosts) // 2, np.sum(topo_matrix) // 2))
def topo_gen(switches, to_hosts, ports, ports_of_virtual_layer, ocs_tx_rx_pairs=320, mat_dir=None):
  
  while True:
    topo_matrix, ports_conn_matrix = virtual_layered_fc(switches, to_hosts, ports, ports_of_virtual_layer, ocs_tx_rx_pairs, mat_dir)
    ideal = switches * (ports - to_hosts) // 2
    real = np.sum(topo_matrix) // 2
    if ideal == real:
      print("successfully return")
      break
  
  if mat_dir is not None:
    print("Write FC mat")
    mat_file = open(mat_dir, mode='w')
    mat_file.write("%d %d %d %d\n" % (to_hosts, switches, (ports - to_hosts) * switches, switches * to_hosts) )
    mat_file.write("%d %s\n" % (len(ports_of_virtual_layer), " ".join(list(map(str, ports_of_virtual_layer) ) ) ) )
    for i in range(switches):
        for j in range(switches):
            if i==j or topo_matrix[i][j]==0: continue
            # print(ports_conn_matrix[i][j])
            mat_file.write("%d %d %d %d\n"%(i, j, ports_conn_matrix[i][j][0], ports_conn_matrix[i][j][1]))
    mat_file.close()

if __name__ == "__main__":
  random.seed(10086)
  
  to_hosts = 24
  ports = 64
  ocs_tx_rx_pairs=320
  
  switches = 500
  ports_of_virtual_layer = [7, 13, 13, 7]
  mat_dir = "../disjoint-path/topo/regular_fc_500_4"
  topo_gen(switches, to_hosts, ports, ports_of_virtual_layer, ocs_tx_rx_pairs, mat_dir)
    
  switches = 1000
  ports_of_virtual_layer = [7, 13, 13, 7]
  mat_dir = "../disjoint-path/topo/regular_fc_1000_4"
  topo_gen(switches, to_hosts, ports, ports_of_virtual_layer, ocs_tx_rx_pairs, mat_dir)
  
  switches = 2000
  ports_of_virtual_layer = [7, 13, 13, 7]
  mat_dir = "../disjoint-path/topo/regular_fc_2000_4"
  topo_gen(switches, to_hosts, ports, ports_of_virtual_layer, ocs_tx_rx_pairs, mat_dir)
  
  switches = 3000
  ports_of_virtual_layer = [7, 13, 13, 7]
  mat_dir = "../disjoint-path/topo/regular_fc_3000_4"
  topo_gen(switches, to_hosts, ports, ports_of_virtual_layer, ocs_tx_rx_pairs, mat_dir)
  
  switches = 5000
  ports_of_virtual_layer = [5, 10, 10, 10, 5]
  mat_dir = "../disjoint-path/topo/regular_fc_5000_5"
  topo_gen(switches, to_hosts, ports, ports_of_virtual_layer, ocs_tx_rx_pairs, mat_dir)
