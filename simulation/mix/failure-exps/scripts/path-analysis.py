#!/usr/bin/env python3
def read_path(path_file='path'):
    path_f = open(path_file, mode='r')
    lines = path_f.read().splitlines()
    path_f.close()
            
    idx = 0
    l0 = [int(x) for x in lines[idx].split(' ')]
    pairs = l0[0]
    host_per_sw = l0[1]
    switches = l0[2]
    # print("sw", switches)

    path_pair = {}
    for i in range(switches):
        path_pair[i] = {}
        for j in range(switches):
            path_pair[i][j] = []
    

    idx += 1
    for _ in range(pairs):
        l = [int(x) for x in lines[idx].split(' ')]
        idx +=1
        src = l[0]
        dst = l[1]
        path_num = l[2]
        for _ in range(path_num):
            path_pair[ src ][ dst ].append([int(x) for x in lines[idx].split(' ')][ 1 : ])
            idx += 1

    pairs = 0.0
    avg_len = 0.0
    avg_shortest_len = 0.0
    avg_num_of_paths = 0.0
    for src in range(switches):
        for dst in range(switches):
            if src == dst: continue
        
            paths = path_pair[src][dst]
            pairs += 1
            avg_num_of_paths += len(paths)
            shortest_len = 0xffff
            sum_len = 0.0
            for path in paths:
                if len(path) < shortest_len: shortest_len = len(path)
                sum_len += len(path)
            avg_len += sum_len / float(len(paths))
            avg_shortest_len += shortest_len
    print("avg number of paths: %.2f average path length: %.2f average shorest path length: %.2f" % (
        avg_num_of_paths / float(pairs),
        avg_len / float(pairs),
        avg_shortest_len / float(pairs)
    ))
    return path_pair

if __name__ == "__main__":
    print('disjoint')
    # read_path("disjoint-path/path/disjoint_500_4")
    # read_path("disjoint-path/path/disjoint_1000_4")
    read_path("disjoint-path/path/disjoint_5000_5")
    
    # print('ECMP')
    # read_path("disjoint-path/path/ecmp_500_4")
    # read_path("disjoint-path/path/ecmp_1000_4")
    # read_path("disjoint-path/path/ecmp_5000_5")
    
    print('edst')
    # read_path("disjoint-path/path/disjoint_500_4")
    # read_path("disjoint-path/path/disjoint_1000_4")
    # read_path("disjoint-path/path/disjoint_5000_5")
