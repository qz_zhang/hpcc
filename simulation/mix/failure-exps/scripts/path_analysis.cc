#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

void read_path(const std::string &path_file) {
    //
    std::ifstream ifs(path_file.c_str());
    // std::unordered_map<int, std::unordered_map<int, std::vector<std::vector<int> > >  pair_paths;
    int pairs, host_per_sw, switches;
    double sum_len = 0.0, sum_shorest_len = 0.0, sum_num = 0.0;
    ifs >> pairs >> host_per_sw >> switches;
    for (int i = 0; i < pairs; i++) {
        int src, dst, path_num;
        double shortest_len = 1666.0, avg_len = 0.0;
        ifs >> src >> dst >> path_num;
        for (int j = 0; j < path_num; j++) {
            int hops;
            ifs >> hops;
            avg_len += hops;
            if (shortest_len > hops) shortest_len = hops;
            std::vector<int> path(hops, 0);
            for (int k = 0; k < hops; k++) {
                ifs >> path[k];
            }
            // It is unnecessary to save the path.
        }
        sum_len += avg_len / (path_num);
        sum_shorest_len += shortest_len;
        sum_num += path_num;
    }
    printf("Average path length: %.2f, average shortest path length: %.2f, average path number: %.2f\n",
        sum_len / pairs,
        sum_shorest_len / pairs,
        sum_num / pairs
    );
    ifs.close();
}
int main() {
    //
    read_path("disjoint-path/path/disjoint_5000_5");
    return 0;
}