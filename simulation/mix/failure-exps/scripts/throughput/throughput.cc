#include "gurobi_c++.h"

#include <vector>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <memory>
#include <fstream>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <thread>
#include <float.h>

#include <unistd.h>

#define FOR_EACH(vec) {\
      for (int l = 0; l < vec.size(); l++) { \
          if (l == vec.size() - 1) { if (vec[l] == INF) std::cout << "INF"; else std::cout << vec[l];} \
          else { if (vec[l] == INF) std::cout << "INF "; else std::cout << vec[l] << " "; } } \
          std::cout << "\n";\
      }

class Edge {
public:
  Edge(int from, int to);
  bool operator==(const Edge& other) const;
  bool operator!=(const Edge& other) const;
  std::string toString() const;
  
  bool operator<(const Edge& other) const{
    return  from_ < other.from_ 
            || from_ == other.from_ && to_ < other.to_;
  }
private:
  int from_;
  int to_;
};

Edge::Edge(int from, int to) 
  : from_(from), to_(to) {}

bool Edge::operator==(const Edge& other) const {
  return from_ == other.from_ && to_ == other.to_;
}

bool Edge::operator!=(const Edge& other) const {
  return !operator==(other);
}

std::string Edge::toString() const{
  std::string edge = "(" + std::to_string(from_) + "," + std::to_string(to_) + ")";
  return edge;
}



void ReadPath(
  const std::string& path_file, 
  std::unordered_map<int, 
    std::unordered_map<int, 
      std::vector<std::vector<int> > > >& path_pairs
) {
  // printf("ReadPath...\n");
  int source_destination_pairs, switches, to_hosts, total_hosts;
	std::ifstream ifs;

	ifs.open(path_file);
	ifs >> source_destination_pairs >> to_hosts >> switches;
	total_hosts = switches * to_hosts;
	// printf("ToRs=%d, to_hosts=%d, total_hosts=%d\n", switches, to_hosts, total_hosts);
	for (int i = 0; i < source_destination_pairs; i++) {
		int src, dst, num_paths;
		ifs >> src >> dst >> num_paths;
    // std::cout << "num_paths: " << num_paths << "\n";
		std::vector<std::vector<int> > paths;
		for (int j = 0; j < num_paths; j++) {
			std::vector<int> path;
			int path_nodes, n;
			ifs >> path_nodes;
			for (int k = 0; k < path_nodes; k++) {
				ifs >> n;
				path.push_back(n);
			}
			paths.push_back(path);
		}
		path_pairs[src][dst] = paths;
	}
	ifs.close();
}


void ReadTrafficMatrix(
  const std::string& traffic_matrix_file,
  std::vector<std::vector<double> >& traffic_matrix
) {
  //
  // std::cout << "Read tm file...\n";
  std::ifstream ifs;
  ifs.open(traffic_matrix_file.c_str());
  int switches;
  ifs >> switches;
  traffic_matrix.resize(switches, std::vector<double>(switches, 0.0));
  for (int i = 0; i < switches; i++) {
    for (int j = 0; j < switches; j++) {
      ifs >> traffic_matrix[i][j];
    }
  }
  ifs.close();
}

//  g++ lp.cc -std=c++11 -I /home/qz/gurobi912/linux64/include/ -L /home/qz/gurobi912/linux64/lib/ -lgurobi_c++ -lgurobi91
double Throughput(
  int switches,
  std::vector<std::vector<double> >& traffic_matrix,
  std::unordered_map<int, 
    std::unordered_map<int, 
      std::vector<std::vector<int> > > >& path_pairs,
  std::string label
) {

  // Edges
  std::set<Edge> edges;
  for (int src = 0; src < switches; src++) {
    for (int dst = 0; dst < switches; dst++) {
      if (src == dst) continue;
      for (auto &path : path_pairs[src][dst]) {
        for (int i = 0; i < path.size() - 1; i++) {
          edges.insert(Edge(path[i], path[i + 1]));
        }
      }
    }
  }

  /*********************
   * Gurobi Model
   ********************/
  GRBEnv *env = new GRBEnv();
  GRBModel model = GRBModel(*env);

  
  GRBVar throughput = model.addVar(0.0, DBL_MAX, 0.0, GRB_CONTINUOUS, "throughput");
  
  std::unordered_map<int, std::unordered_map<int, GRBVar * > > flow_vars;
  for (int i = 0; i < switches; i++) {
    for (int j = 0; j < switches; j++) {
      if ( path_pairs[i][j].size() == 0) continue;
      int path_num = path_pairs[i][j].size();
      
      flow_vars[i][j] = model.addVars(path_num, GRB_CONTINUOUS);
      
      // Set attribute of variables
      for (int k = 0; k < path_num; k++) {
        flow_vars[i][j][k].set(GRB_DoubleAttr_LB, 0.0);
        flow_vars[i][j][k].set(GRB_DoubleAttr_UB, DBL_MAX);
      }
    }
  }


  // add constraints
  for (int i = 0; i < switches; i++) { // src_host
    for (int j = 0; j < switches; j++) { // dst_host
      if (i == j) continue;
      GRBLinExpr constr = 0;
      for (int k = 0; k < path_pairs[i][j].size(); k++) {
        constr += flow_vars[i][j][k];
      }
      constr -= throughput * traffic_matrix[i][j];
      model.addConstr(constr, GRB_EQUAL, 0);
    }
  }

  /**
   * The sum of flow traverse to a single link cannot exceeds bandwidth
   */
  for (auto & e : edges) {
    GRBLinExpr constr = 0;
    for (int i = 0; i < switches; i++) { // src_host
      for (int j = 0; j < switches; j++) { // dst_host
        if (path_pairs[i][j].size() == 0) continue;
        const auto &paths = path_pairs[i][j];
       
        for (int k = 0; k < paths.size(); k++) { // i -> j has path num's path
          bool has_edge = false;
          for (int l = 0; l < paths[k].size() - 1; l++) {
            // edge: paths[k][l]->paths[k][l+1]
            Edge edge_compared(paths[k][l], paths[k][l+1]);
            if (e == edge_compared) {
              has_edge = true;
              break;
            }
          }
          // has Edge
          if (has_edge) {
            constr += flow_vars[i][j][k];
          }
        }

      }
    }
    model.addConstr(constr, GRB_LESS_EQUAL, 1);
  }
  
  model.setObjective(1 * throughput, GRB_MAXIMIZE);
  model.set(GRB_IntParam_OutputFlag, 0);
  // simplex use less memory
  // model.set(GRB_IntParam_Method, 0);
  // std::cout << "Optimizing...\n";
  double th = 0.0;
  try {
    model.optimize();
    if(model.get(GRB_IntAttr_Status) != GRB_OPTIMAL){
      std::cout << "not optimal " << std::endl;
    } else {
      th = model.get(GRB_DoubleAttr_ObjVal);
      // printf("%s throughput: %.6f\n", label.c_str(), model.get(GRB_DoubleAttr_ObjVal));
    }
  } catch (GRBException e) {
    std::cout << "Error code = " << e.getErrorCode() << std::endl;
    std::cout << e.getMessage() << std::endl;
  }

  // Prevent memory leaks
  for (int i = 0; i < switches; i++)
    for (int j = 0; j < switches; j++)
      if (flow_vars[i][j] != NULL) {
        delete[] flow_vars[i][j];
      }

  delete env;
  return th;
}


// std::vector<std::vector<double> > a2a_traffic_matrix, worst_traffic_matrix;
// std::unordered_map<int, 
//   std::unordered_map<int, 
//     std::vector<std::vector<int> > > > ecmp_path_pairs, disjoint_path_pairs, edst_path_pairs, path_pairs;

void thread_task(
  std::vector<int> switches, 
  std::vector<int> ks, 
  const std::string routing="ecmp"
) {
  printf("Start %s-thread...\n", routing.c_str());
  char str_buf[256];
  int exp_threshold = 1;
  std::vector<std::vector<double> > a2a_traffic_matrix, worst_traffic_matrix;
  std::unordered_map<int, 
    std::unordered_map<int, 
      std::vector<std::vector<int> > > > path_pairs;
  
  for (int i = 0; i < switches.size(); i++) {
    int switch_num = switches[i];
    int k = ks[i];
    std::cout << "SW: " << switch_num << "\n";
    int num_exps = 0;
    if (switch_num <= 500) {
      num_exps = exp_threshold;
    } else {
      num_exps = 1;
    }
    double worst_avg_th = 0.0, a2a_avg_th = 0.0;
    for (int j = 0; j < num_exps; j++) {

      sprintf(str_buf, "path/%s_%d_%d_d32", routing.c_str(), switch_num, k);
      std::string path_file(str_buf);
      sprintf(str_buf, "tm/a2a_%d_%d_d32", switch_num, k);
      std::string a2a_tm_file(str_buf);
      sprintf(str_buf, "tm/worst_%d_%d_d32", switch_num, k);
      std::string worst_tm_file(str_buf);
      
      sprintf(str_buf, "WORST %s %d", routing.c_str(), switch_num);
      std::string worst_label(str_buf);

      sprintf(str_buf, "A2A %s %d", routing.c_str(), switch_num);
      std::string a2a_label(str_buf);
      
      // clear container
      path_pairs.clear();
      worst_traffic_matrix.clear();
      a2a_traffic_matrix.clear();
      
      ReadPath(path_file, path_pairs);
      ReadTrafficMatrix(a2a_tm_file, a2a_traffic_matrix);
      ReadTrafficMatrix(worst_tm_file, worst_traffic_matrix);
      worst_avg_th += Throughput(switch_num, worst_traffic_matrix, path_pairs, worst_label);
      a2a_avg_th += Throughput(switch_num, a2a_traffic_matrix, path_pairs, a2a_label);
    }
    printf("SWITCHES %d A2A %.6f WORST %.6f %s\n", switch_num, a2a_avg_th / num_exps, worst_avg_th / num_exps, routing.c_str());
  }
}

/**
void multi_thread(int switches, int k, int sed) {

  std::vector<std::unique_ptr< std::thread > > threads;
  for (int i = 0; i < switches.size(); i++) {
    // string fmt
    char strBuf[256];
    
    sprintf(strBuf, "path/ecmp_%d_%d_d32", switches, k);
    std::string ecmp_path_file(strBuf);

    sprintf(strBuf, "path/disjoint_%d_%d_%d", switches, k);
    std::string disjoint_path_file(strBuf);

    sprintf(strBuf, "path/edst_%d_%d_%d", switches, k);
    std::string edst_path_file(strBuf);

    sprintf(strBuf, "tm/a2a_%d_%d_d32", switches, k);
    std::string a2a_tm_file(strBuf);

    sprintf(strBuf, "tm/worst_%d_%d_d32", switches, k);
    std::string worst_tm_file(strBuf);

    // init label
    sprintf(strBuf, "WORST ECMP-LP %d", switches);
    std::string worst_ecmp_label(strBuf);

    sprintf(strBuf, "WORST DISJOINT-LP %d", switches);
    std::string worst_disjoint_label(strBuf);

    sprintf(strBuf, "WORST EDST-LP %d", switches);
    std::string worst_edst_label(strBuf);

    sprintf(strBuf, "A2A ECMP-LP %d", switches);
    std::string a2a_ecmp_label(strBuf);

    sprintf(strBuf, "A2A DISJOINT-LP %d", switches);
    std::string a2a_disjoint_label(strBuf);

    sprintf(strBuf, "A2A EDST-LP %d", switches);
    std::string a2a_edst_label(strBuf);
    
    std::cout << ecmp_path_file << " " << disjoint_path_file << " " << edst_path_file << "\n";
    // clear container
    a2a_traffic_matrix.clear();
    worst_traffic_matrix.clear();
    ecmp_path_pairs.clear();
    disjoint_path_pairs.clear();
    edst_path_pairs.clear();

    // ReadPath
    ReadPath(ecmp_path_file, ecmp_path_pairs);
    ReadPath(disjoint_path_file, disjoint_path_pairs);
    ReadPath(edst_path_file, edst_path_pairs);
    ReadTrafficMatrix(a2a_tm_file, a2a_traffic_matrix);
    ReadTrafficMatrix(worst_tm_file, worst_traffic_matrix);

    // std::cout << "A2A ECMP\n";
    Throughput(a2a_traffic_matrix, ecmp_path_pairs, a2a_ecmp_label);
    
    // std::cout << "A2A DISJOINT\n";
    Throughput(a2a_traffic_matrix, disjoint_path_pairs, a2a_disjoint_label);
    
    // std::cout << "A2A EDST\n";
    Throughput(a2a_traffic_matrix, edst_path_pairs, a2a_edst_label);
    
    // std::cout << "WORST ECMP\n";
    Throughput(worst_traffic_matrix, disjoint_path_pairs, worst_disjoint_label);
    
    // std::cout << "WORST DISJOINT\n";
    Throughput(worst_traffic_matrix, ecmp_path_pairs, worst_ecmp_label);
    
    std::cout << "WORST EDST\n";
    Throughput(worst_traffic_matrix, edst_path_pairs, worst_edst_label);
    threads.push_back(std::unique_ptr< std::thread >(new std::thread(Throughput, std::ref(a2a_traffic_matrix), std::ref(ecmp_path_pairs), a2a_ecmp_label)));
    threads.push_back(std::unique_ptr< std::thread >(new std::thread(Throughput, std::ref(a2a_traffic_matrix), std::ref(disjoint_path_pairs), a2a_disjoint_label)));
    threads.push_back(std::unique_ptr< std::thread >(new std::thread(Throughput, std::ref(a2a_traffic_matrix), std::ref(edst_path_pairs), a2a_edst_label)));
    
    threads.push_back(std::unique_ptr< std::thread >(new std::thread(Throughput, std::ref(worst_traffic_matrix), std::ref(ecmp_path_pairs), worst_ecmp_label)));
    threads.push_back(std::unique_ptr< std::thread >(new std::thread(Throughput, std::ref(worst_traffic_matrix), std::ref(disjoint_path_pairs), worst_disjoint_label)));
    threads.push_back(std::unique_ptr< std::thread >(new std::thread(Throughput, std::ref(worst_traffic_matrix), std::ref(edst_path_pairs), worst_edst_label)));

  }

  for (auto &thread : threads) {
    thread->join();
  }
}
**/

int main(int argc, char **argv) {
  std::cout << "PID: " << getpid() << "\n";
  // std::vector<int> switches = {50, 100, 150, 200, 250, 300};
  // std::vector<int>       ks = {4,   4,   4,   4,    4,   4};

  std::vector<int> switches = {400, 500};
  std::vector<int>       ks = {4,   4};
  
  // std::vector<int> switches1 = {1000};
  // std::vector<int> ks1      = {4};

  // std::vector<int> switches2 = {800, 1000};
  // std::vector<int> ks2      = {4, 4};

  // thread_task(switches1, ks1, "ecmp");
  // thread_task(switches2, ks2, "disjoint");
  //thread_task(switches1, ks1, "ksp");
  thread_task(switches, ks, "ecmp");
  thread_task(switches, ks, "disjoint");
  thread_task(switches, ks, "edst");
  thread_task(switches, ks, "ksp16");
  // thread_task(switches, ks, "ksp16");
  // std::vector<std::unique_ptr< std::thread > > threads;
  // threads.push_back(std::unique_ptr< std::thread >(new std::thread(thread_task, switches1, ks1, "ecmp")));
  // // threads.push_back(std::unique_ptr< std::thread >(new std::thread(thread_task, switches, ks, "disjoint")));
  // for (auto &thread : threads) {
  //   thread->join();
  // }
  // for (int i = 0; i < 10; i++) {
  //   int sed = 2 * i + 1;
  //   multi_thread(100, 3);
  //   multi_thread(200, 3);
  //   multi_thread(300, 3);
  //   // multi_thread(800, 4);
  //   // multi_thread(1000, 4);
  //   std::cout << "\n";
  // }

  return 0;
}
