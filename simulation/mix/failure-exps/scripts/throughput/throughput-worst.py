#!/usr/bin/env python3
from gurobipy import *

import numpy as np
import os
import random
from multiprocessing import Process
import networkx as nx
from throughput import Throughput
import sys
sys.path.append('TUB')
from topo_repo import topology
# from throughput import Throughput
random.seed(10086)
class Edge:
    def __init__(self, nodeA, nodeB):
        self.nodeA = nodeA
        self.nodeB = nodeB
        self.total_traffic = 0
    def add_traffic(self, traffic):
        self.total_traffic += traffic

    def __hash__(self):
        return hash(self.nodeA) + hash(self.nodeB)

    def __eq__(self, other):
        return  type(self) == type(other) and self.nodeA == other.nodeA and self.nodeB == other.nodeB 
    
    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return f'({self.nodeA},{self.nodeB})'

def get_worst_tm_and_tub(topo_type, switches, topo_matrix, hosts):
    G = nx.Graph()
    for src in range(switches):
        for dst in range(switches):
            if topo_matrix[src][dst] == 1:
                G.add_edge(src, dst)
    
    tor_list = [tor_sid for tor_sid in range(switches)]
    demand_list = {}
    for sid in range(switches):
        demand_list[sid] = hosts

    topo = topology.Topology(G, tor_list, demand_list)
    tub = topo.get_tub()
    # print("topo_type=%s hosts=%s, TUB=%.2f" % (topo_type, hosts, tub))

    worst_case_tm = np.zeros((switches, switches))
    
    ## tm is a dictionary
    tm, _ = topo.get_near_worst_case_traffic_matrix()

    for key in tm.keys():
        src = key[0]
        dst = key[1]
        # print(tm[key])
        worst_case_tm[src][dst] = tm[key]
    return worst_case_tm, tub

def gen_all_to_all_TM(switches, switch_recv_bandwidth):
    tm = np.zeros(shape=(switches, switches))
    for i in range(switches):
        for j in range(switches):
            if i == j: continue
            # tm[i][j] = 1
            tm[i][j] = switch_recv_bandwidth / float(switches - 1)
    # print(tm)
    return tm


def path_calculation(type, mat_dir, path_dir, k):
    if type == "ksp": os.system("./ksp %s %s " % (mat_dir, path_dir))
    else: os.system("./edst %s %s %d" % (mat_dir, path_dir, k))

def gen_all_to_all_TM(switches, switch_recv_bandwidth):
    tm = np.zeros(shape=(switches, switches))
    for i in range(switches):
        for j in range(switches):
            if i == j: continue
            # tm[i][j] = 1
            tm[i][j] = switch_recv_bandwidth / float(switches - 1)
    # print(tm)
    return tm


def read_path(path_file='path'):
    print(path_file)
    path_f = open(path_file, mode='r')
    lines = path_f.read().splitlines()
    path_f.close()
            
    idx = 0
    l0 = [int(x) for x in lines[idx].split(' ')]
    pairs = l0[0]
    host_per_sw = l0[1]
    switches = l0[2]
    # print("sw", switches)

    path_pair = {}
    for i in range(switches):
        path_pair[i] = {}
        for j in range(switches):
            path_pair[i][j] = []
    

    idx += 1
    for _ in range(pairs):
        l = [int(x) for x in lines[idx].split(' ')]
        idx +=1
        src = l[0]
        dst = l[1]
        path_num = l[2]
        for _ in range(path_num):
            path_pair[ src ][ dst ].append([int(x) for x in lines[idx].split(' ')][ 1 : ])
            idx += 1
    return path_pair

def read_topo(mat_file):
    mat_f = open(mat_file, mode='r')
    lines = mat_f.read().splitlines()
    to_host, switches, sw2sw_links, servers = map(int, lines[0].split())
    topo_matrix = np.zeros((switches, switches))
    # print('switch=', switches)
    for line in lines[1:]:
        data = list(map(int, line.split()))
        topo_matrix[data[0]][data[1]] = 1
        topo_matrix[data[1]][data[0]] = 1
    return topo_matrix

def throughput(traffic_matrix, path_pair,  label, p=0.7):
    
    switches = len(traffic_matrix)
    running_traffic = np.zeros((switches, switches), dtype=float)
    
    for src in range(switches):
        for dst in range(switches):
            if src == dst: continue
            paths = path_pair[src][dst]
            
            shortest_path_len = len(paths[0])
            total_hop = 0
            for path in paths:
                total_hop += len(path)
                if shortest_path_len > len(path): shortest_path_len = len(path)
            """
            ## check the shortest path len.
            
            short_path_num = 0
            longer_path_num = 0
            ratio_to_short_path = 0
            ratio_to_longer_path = 0
            for path in paths:
                if len(path) == shortest_path_len: short_path_num += 1
            
            longer_path_num = len(paths) - short_path_num
            
            if longer_path_num == 0: 
                ratio_to_longer_path = ratio_to_short_path = 1 / float(short_path_num)
            else:
                ratio_to_short_path = p / float(short_path_num)
                ratio_to_longer_path = (1 - p) / float(longer_path_num)
            """
            ratio_to_short_path = ratio_to_longer_path = 1 / float(len(paths))
            # path_ratio = [0.0 for i in range(num_paths)]
            # for i in range(len(paths)):
            #     path_ratio[i] = len(paths[i]) / float(total_hop)
            
            # for j in range(len(paths)):
            #     path = paths[j]
            #     if len(path) == shortest_path_len:
            #         for i in range(len(path) - 1):
            #             running_traffic[path[i]][path[i + 1]] +=  traffic_matrix[src][dst] * path_ratio[j]
            #     else:
            #         for i in range(len(path) - 1):
            #             running_traffic[path[i]][path[i + 1]] +=  traffic_matrix[src][dst] * path_ratio[j]
            
            for j in range(len(paths)):
                path = paths[j]
                if len(path) == shortest_path_len:
                    for i in range(len(path) - 1):
                        running_traffic[path[i]][path[i + 1]] +=  traffic_matrix[src][dst] * ratio_to_short_path
                else:
                    for i in range(len(path) - 1):
                        running_traffic[path[i]][path[i + 1]] +=  traffic_matrix[src][dst] *  ratio_to_longer_path
    print("%s throughput = %.2f " % (label, 1.0 / np.max(running_traffic)))

def worst_throughput(p = 0.8):

    # switches = [100, 200, 300]
    # corresponding_ks = [3, 3, 3]
    
    switches = [500, 1000]
    corresponding_ks = [4, 4]
    """
    s2
    """
    to_hosts = 24
    worst_processes = []
    for i in range(len(switches)):
        switch = switches[i]
        k = corresponding_ks[i]
        fc_mat_dir = "disjoint-path/topo/fc_%d_%d" % (switch, k)
        
        fc_disjoint_path_dir = "disjoint-path/path/disjoint_%d_%d" % (switch, k)
        fc_ecmp_path_dir = "disjoint-path/path/ecmp_%d_%d" % (switch, k)
        fc_edst_path_dir = "disjoint-path/path/edst_%d_%d" % (switch, k)
        
        fc_topo_matrix = read_topo(fc_mat_dir)
        fc_ecmp_path = read_path(fc_ecmp_path_dir)
        fc_edst_path = read_path(fc_edst_path_dir)
        fc_disjoint_path = read_path(fc_disjoint_path_dir)
        
        fc_worst_case_tm, _ = get_worst_tm_and_tub("FC", switch, fc_topo_matrix, to_hosts)
        
        
        if switch <= 300:
            worst_processes.append(Process(target=Throughput, args=(fc_worst_case_tm, fc_disjoint_path, "WORST %d FC DISJOINT-LP" % (switch))))
            worst_processes.append(Process(target=Throughput, args=(fc_worst_case_tm, fc_ecmp_path, "WORST %d FC ECMP-LP" % (switch))))
            worst_processes.append(Process(target=Throughput, args=(fc_worst_case_tm, fc_edst_path, "WORST %d FC EDST-LP" % (switch))))
        else:
            worst_processes.append(Process(target=throughput, args=(fc_worst_case_tm, fc_disjoint_path, "WORST %d FC DISJOINT" % (switch))))
            worst_processes.append(Process(target=throughput, args=(fc_worst_case_tm, fc_ecmp_path, "WORST %d FC ECMP" % (switch))))
            worst_processes.append(Process(target=throughput, args=(fc_worst_case_tm, fc_edst_path, "WORST %d FC EDST" % (switch))))
        
        print("A2A Traffic Pattern...")
        a2a_tm = gen_all_to_all_TM(switch, to_hosts)
        if switch <= 300:
            worst_processes.append(Process(target=Throughput, args=(a2a_tm, fc_disjoint_path, "A2A %d FC DISJOINT-LP" % (switch))))
            worst_processes.append(Process(target=Throughput, args=(a2a_tm, fc_ecmp_path, "A2A %d FC ECMP-LP" % (switch))))
            # worst_processes.append(Process(target=Throughput, args=(a2a_tm, fc_edst_path, "A2A %d FC EDST-LP" % (switch))))
            # worst_processes.append(Process(target=Throughput, args=(a2a_tm, fc_up_down_path, "A2A %d FC LP-UP_DOWN" % (switches))))
        else:
            worst_processes.append(Process(target=throughput, args=(a2a_tm, fc_disjoint_path, "A2A %d FC DISJOINT" % (switch))))
            worst_processes.append(Process(target=throughput, args=(a2a_tm, fc_ecmp_path, "A2A %d FC ECMP" % (switch))))
            worst_processes.append(Process(target=throughput, args=(a2a_tm, fc_edst_path, "A2A %d FC EDST" % (switch))))

    for wp in worst_processes:
        wp.start()

    for wp in worst_processes:
        wp.join()

def write_tm():
    switches = [i * 50 for i in range(1, 7)]
    ks = [4 for _ in range(6)]
    switches = [400, 500]
    ks = [4, 4]

    to_hosts = 8
    worst_tm_fmt = "tm/worst_%d_%d_d32"
    a2a_tm_fmt = "tm/a2a_%d_%d_d32"
    for switch_idx in range(len(switches)):
        switch = switches[switch_idx]
        k = ks[switch_idx]
        fc_mat_dir = "topo/fc_%d_%d_d32" % (switch, k)
        fc_topo_matrix = read_topo(fc_mat_dir)
        worst_case_tm, _ = get_worst_tm_and_tub("FC", switch, fc_topo_matrix, to_hosts)
        a2a_tm = gen_all_to_all_TM(switch, to_hosts)
        worst_tm_dir = worst_tm_fmt % (switch, k)
        a2a_tm_dir   = a2a_tm_fmt % (switch, k)
        worst_tm_f = open(worst_tm_dir, mode='w')
        a2a_tm_f = open(a2a_tm_dir, mode='w')
        worst_tm_f.write("%d\n" % ( switch))
        a2a_tm_f.write("%d\n" % (switch))
        for i in range(switch):
            worst_tm_f.write("%s\n" % ( " ".join(map(str, worst_case_tm[i]))) )
            a2a_tm_f.write("%s\n" % (" ".join(map(str, a2a_tm[i]))) )
        
if __name__ == "__main__":
    write_tm()
    # worst_throughput()
