import os

if __name__ == "__main__":
    # os.system('. run.sh')
    switches = [50, 100, 150, 200, 250, 300]
    switches = [400, 500]
    ks = [4,4]
    ks = [4,4]

    print('calculate disjoint path...')
    for idx in range(len(switches)):
        switch = switches[idx]
        k = ks[idx]
        topo_file = "topo/fc_%d_%d_d32" % (switch, k)
        path_ofile = "path/disjoint_%d_%d_d32" % (switch, k)
        os.system('./disjoint %s %s' % (topo_file, path_ofile))

    print("calculate edst path...")
    for idx in range(len(switches)):
        switch = switches[idx]
        k = ks[idx]
        topo_file = "topo/fc_%d_%d_d32" % (switch, k)
        path_ofile = "path/edst_%d_%d_d32" % (switch, k)
        os.system('./edst %s %s 12' % (topo_file, path_ofile))

                
    print("calculate KSP path...")
    k_paths = 16
    for idx in range(len(switches)):
        switch = switches[idx]
        k = ks[idx]
        topo_file = "topo/fc_%d_%d_d32" % (switch, k)
        path_ofile = "path/ksp%d_%d_%d_d32" % (k_paths, switch, k)
        os.system('./ksp %s %s %d' % (topo_file, path_ofile, k_paths))
